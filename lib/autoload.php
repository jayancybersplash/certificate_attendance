<?php
/**
 * check PHP vesion support
 */
//define ('VERSION_MIN', '7.0');
define ('VERSION_MIN', '5.6');
if (true === version_compare(PHP_VERSION, VERSION_MIN, '<')) {
    die('This application require PHP version "' . VERSION_MIN . '" or higher.');
}

/**
 * Autoload configurations
 */
define ('DS', '/');
define ('WDS', '\\');
define ('BS', '\\');
$windows = preg_match('/win/i', PHP_OS);
define ('OS_WINDOWS', $windows);

/**
 * fix Windows directory path
 * 
 * @param string $path
 * @return string
 */
function fwdp($path)
{
    if (1 == OS_WINDOWS) {
        return str_replace(WDS, DS, $path);
    }
    return $path;
}

/**
 * application configurations and settings
 */
ini_set ('default_charset', 'UTF-8');
ini_set ('display_errors', 'on');
error_reporting(E_ALL);
date_default_timezone_set('Asia/Dubai');
define ('LIB_DIR', fwdp(__DIR__));
define ('WEB_DIR', LIB_DIR . DS . 'web');
define ('CLI_DIR', LIB_DIR . DS . 'cli');
define ('MODULE_DIR', LIB_DIR . DS . 'module');
define ('MODEL_DIR', LIB_DIR . DS . 'model');
define ('VENDOR_DIR', LIB_DIR . DS . 'vendor');
define ('MEDIA_URL', 'media/');
define ('STORAGE_DIR', LIB_DIR . DS . 'storage');
define ('UPLOAD_DIR', APP_DIR . DS . 'assets');
define ('UPLOAD_URL', 'assets/');

if ('cli' === PHP_SAPI) {
    define('CLI', 1);
    $paths = [
        'Web' => WEB_DIR . DS . 'Web/src',
        'Cli' => CLI_DIR . DS . 'Cli/src',
        'Model' => MODEL_DIR . DS . 'Model/src',
    ];
} else {
    $paths = [
        'Web' => WEB_DIR . DS . 'Web/src',
        'Application' => MODULE_DIR . DS . 'Application/src',
        'Model' => MODEL_DIR . DS . 'Model/src',
    ];
}

// set vendor packages
$vendors = [
    'PHPMailer' => 'src',
];

//vendors with special namespace
$customVendors = [];

require LIB_DIR . DS . 'loader.php';

$loader = new PSR4ClassAutoloader();
foreach ($paths as $namespace => $path) {
    $loader->registerNamespace($namespace, $path);
}

if (! empty($vendors)) {
    foreach ($vendors as $namespace => $source) {
        $loader->registerVendor($namespace, $source);
    }
}

if (! empty($customVendors)) {
    foreach ($customVendors as $vendor) {
        $loader->registerVendorNamespace($vendor[0], $vendor[1]);
    }
}

$loader->registerAutoload();
