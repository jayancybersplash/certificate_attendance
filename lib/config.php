<?php
/**
 * Configurations
 */

return [
    'modules' => [
        'Application' => '',
    ],
    'registry' => [
        'project' => 'Certificate',
        'siteName' => 'Certificate',
    ],
];
