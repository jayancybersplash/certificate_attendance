<?php
/**
 * Cli Configurations
 */

return [
    'environment' => 'development',
    'registry' => [
        'project' => 'Certificate',
        'siteName' => 'Certificate',
        'siteUrl' => 'http://www.certificate.loc/',
        'siteDomain' => 'www.certificate.loc',
        'baseUrl' => 'http://www.certificate.loc/',
        'actionUrl' => 'http://www.certificate.loc/',
        'ssl' => 0,
    ],
];
