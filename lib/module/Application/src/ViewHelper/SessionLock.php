<?php
/**
 * SessionLock View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;


class SessionLock extends ViewHelper
{
    /**
     * get session locked flag
     * 
     * @param int|boolean $locked
     * @return string
     */
    public function sessionLock($locked = false)
    {
        if ($locked) {
            return '<span class="red"> &nbsp; [ LOCKED ] </span>';
        }
        
        return '';
    }
    
}
