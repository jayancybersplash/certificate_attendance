<?php
/**
 * SessionDate View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Application\Utility\SessionDateHelper;

use Web\View\ViewHelper;


class SessionDate extends ViewHelper
{
    /**
     * get session Date
     * 
     * @param string $start
     * @param string $end
     * @param string $separator
     * @return string
     */
    public function sessionDate($start, $end = null, $separator = ' - ')
    {
        if (null === $end) {
            return SessionDateHelper::view($start);
        }
        
        if ($end == $start) {
            return SessionDateHelper::view($start);
        }
        
        return SessionDateHelper::view($start) . $separator . SessionDateHelper::view($end);
    }
    
}
