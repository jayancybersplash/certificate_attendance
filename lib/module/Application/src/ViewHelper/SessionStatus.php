<?php
/**
 * SessionStatus View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;
use Web\Utility\DateHelper;


class SessionStatus extends ViewHelper
{
    /**
     * get Session Status
     * 
     * @param string $startDate
     * @param int $locked
     * @param string $future
     * @return string
     */
    public function sessionStatus($startDate, $locked = 0, $future = '--')
    {
        if ($locked > 0) {
            return '<span class="badge badge-lg badge-danger">Locked</span>';
        }
        //Checking given date is future date
        if (DateHelper::isFutureDate($startDate)) {
            return $future;
        } 
        
        return '<span class="badge badge-lg badge-success">Open</span>';
    }
    
}
