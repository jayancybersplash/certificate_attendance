<?php
/**
 * ModuleName View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;


class ModuleName extends ViewHelper
{
    const PATTERN = '"%s" - %s';
    
    /**
     * get formatted module name
     * 
     * @param string $code
     * @param string $title
     * @param boolean $escape
     * @return string
     */
    public function moduleName($code, $title, $escape = true)
    {
        $name = sprintf(self::PATTERN, $code, $title);
        if ($escape) {
            return $this->escape($name);
        }
        
        return $name;
    }
    
}
