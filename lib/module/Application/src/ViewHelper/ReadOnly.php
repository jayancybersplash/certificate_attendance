<?php
/**
 * ReadOnly View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;


class ReadOnly extends ViewHelper
{
    /**
     * get readonly attribute
     * 
     * @param boolean $readOnly
     * @return string
     */
    public function readOnly($readOnly = false)
    {
        if ($readOnly) {
            return ' readonly="readonly"';
        }
        
        return '';
    }
    
}
