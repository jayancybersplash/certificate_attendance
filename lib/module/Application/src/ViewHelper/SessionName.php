<?php
/**
 * SessionName View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Application\Utility\SessionDateHelper;

use Web\View\ViewHelper;


class SessionName extends ViewHelper
{
    /**
     * get formatted session name
     * 
     * @param \stdClass $session
     * @return string
     */
    public function sessionName($session)
    {
        $sections = [];
        
        if ($session->start_date == $session->end_date) {
            $sections[] = SessionDateHelper::view($session->start_date);
        } else {
            $sections[] = SessionDateHelper::view($session->start_date) . ' - '
                        . SessionDateHelper::view($session->end_date);
        }
        $sections[] = $session->title;
        $sections[] = $session->location;
        
        return $this->escape(implode(', ', $sections));
    }
    
}
