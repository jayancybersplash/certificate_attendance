<?php
/**
 * Display Style View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;


class DisplayStyle extends ViewHelper
{
    /**
     * get display style
     * 
     * @param boolean $hide
     * @return string
     */
    public function displayStyle($hide = true)
    {
        if ($hide) {
            return ' style="display:none;"';
        }
        
        return '';
    }
    
}
