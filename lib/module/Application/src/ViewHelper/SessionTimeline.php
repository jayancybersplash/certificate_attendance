<?php
/**
 * SessionTimeline View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;
use Web\Utility\DateHelper;


class SessionTimeline extends ViewHelper
{
    /**
     * get Session Timeline
     * 
     * @param string $startDate
     * @param string $endDate
     * @return string
     */
    public function sessionTimeline($startDate, $endDate)
    {
        $badge = '<span class="badge badge-lg badge-%s">%s</span>';
        if (DateHelper::isPastDate($endDate)) {
            return sprintf($badge, 'info', 'Completed');
        }
        if (DateHelper::isFutureDate($startDate)) {
            return sprintf($badge, 'primary', 'Upcoming');
        }
        
        return sprintf($badge, 'success', 'Ongoing');
    }
    
}
