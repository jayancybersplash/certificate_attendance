<?php
/**
 * Disabled View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;


class Disabled extends ViewHelper
{
    /**
     * get disabled attribute
     * 
     * @param boolean $disabled
     * @return string
     */
    public function disabled($disabled = false)
    {
        if ($disabled) {
            return ' disabled="disabled"';
        }
        
        return '';
    }
    
}
