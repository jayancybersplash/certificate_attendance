<?php
/**
 * AttendanceMark View helper
 * 
 * @category   Application
 * @package    Application\ViewHelper
 */

namespace Application\ViewHelper;

use Web\View\ViewHelper;


class AttendanceMark extends ViewHelper
{
    /**
     * get Attendance Mark
     * 
     * @param string $startDate
     * @param string $endDate
     * @return string
     */
    public function attendanceMark($item)
    {
        
        $mark = [
            0 => '',
            1 => '<span class="ft-check primary"></span>',
        ];

        return $mark[$item];
    }
    
}
