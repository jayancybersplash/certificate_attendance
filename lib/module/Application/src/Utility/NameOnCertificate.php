<?php

/**
 * NameOnCertificate Utility
 *
 * @category   Application
 * @package    Application\Utility
 */

namespace Application\Utility;


class NameOnCertificate
{
    /**
     *
     * @var string 
     */
    private $_firstName;
    
    /**
     *
     * @var string 
     */
    private $_lastName;
    
    public function __construct($firstName = '', $lastName = '')
    {
       $this->_firstName = $firstName;
       $this->_lastName = $lastName;
    }
    
    /**
     * set first name
     * 
     * @param string $name
     * @return $this
     */
    public function setFirstName($name)
    {
        $this->_firstName = $name;
        
        return $this;
    }
    
    /**
     * set last name
     * 
     * @param string $name
     * @return $this
     */
    public function setLastName($name)
    {
        $this->_lastName = $name;
        
        return $this;
    }
    
    /**
     * get name on certificate
     * 
     * @param string $firstName
     * @param string $lastName
     * @return string
     */
    public function get($firstName = null, $lastName = null)
    {
        if (null !== $firstName) {
            $this->setFirstName($firstName);
        }
        
        if (null !== $lastName) {
            $this->setLastName($lastName);
        }
        
        return $this->_build();
    }
    
    /**
     * build name on certificate
     * 
     * @return string
     */
    private function _build()
    {
        $name = trim($this->_firstName)
              . ' ' . trim($this->_lastName);
        
        return strtoupper($name);
    }
    
}
