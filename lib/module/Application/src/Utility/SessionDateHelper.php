<?php
/**
 * SessionDateHelper Utility
 *
 * @category   Application
 * @package    Application\Utility
 */

namespace Application\Utility;

use Application\Constant\SessionConstant;
use Web\Utility\DateHelper;


class SessionDateHelper
{
    /**
     * get date formatted for view
     * 
     * @param string $date
     * @return string
     */
    public static function view($date)
    {
        return DateHelper::convert($date, SessionConstant::DATEPICKER_FORMAT_PHP, DateHelper::MYSQL_DATE);
    }
    
    /**
     * check datepicker date value format
     * 
     * @param string $date
     * @return boolean
     */
    public static function checkDatepicker($date)
    {
        return DateHelper::checkFormat($date, SessionConstant::DATEPICKER_FORMAT_PHP);
    }
    
    /**
     * check date range
     * 
     * @param string $start
     * @param string $end
     * @param boolean $allowSameDate
     * @return boolean
     */
    public static function checkRange($start, $end, $allowSameDate = true)
    {
        if ('' == $start || '' == $end) {
            return false;
        }
        
        if ($start == $end) {
            return $allowSameDate;
        }
        
        return DateHelper::fromFormat($end, SessionConstant::DATEPICKER_FORMAT_PHP) > DateHelper::fromFormat($start, SessionConstant::DATEPICKER_FORMAT_PHP);
    }
    
    /**
     * convert date to MySQL DATE format
     * 
     * @param string $date
     * @return string
     */
    public static function toSql($date)
    {
        return DateHelper::convert($date, DateHelper::MYSQL_DATE, SessionConstant::DATEPICKER_FORMAT_PHP);
    }
    
}
