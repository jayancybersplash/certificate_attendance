<?php

/**
 * CertificateWriter Utility
 *
 * @category   Application
 * @package    Application\Utility
 */

namespace Application\Utility;

use DateTime;


class CertificateWriter
{
    const FONT_SIZE = 40;
    const POSITION_X = 10;
    const POSITION_Y = 67;
    
    /**
     *
     * @var string 
     */
    private $_name;
    
    /**
     *
     * @var string 
     */
    private $_template;
    
    /**
     *
     * @var string 
     */
    private $_tempatePath;
    
    /**
     *
     * @var string 
     */
    private $_error;
    
    /**
     *
     * @var string 
     */
    private $_certificatePath;
    
    public function __construct()
    {
        $this->_tempatePath = UPLOAD_DIR . '/templates';
    }
    
    /**
     * set name
     * 
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->_name = $name;
        
        return $this;
    }
    
    /**
     * set template
     * 
     * @param string $template
     * @return $this
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
        
        return $this;
    }
    
    /**
     * generate certificate
     * 
     * @param int $sessionId
     * @return boolean|string
     */
    public function run($sessionId = 0)
    {
        if ('' == $this->_template || '' == $this->_name) {
            $this->_error = 'Template and/or name is missing';
            return false;
        }
        
        $template = $this->_tempatePath . '/' . $this->_template;
        if (! is_file($template)) {
            $this->_error = 'Template file "' . $this->_template . '" not found';
            return false;
        }
        
        $this->_checkCertificateDirectory($sessionId);
        if (! is_dir($this->_certificatePath)) {
            $this->_error = 'Certificate svae directory not found';
            return false;
        }
        
        $this->_loadPdfLibrary();
        
        $pdf = new \FPDI('l');
        $pdf->setSourceFile($template);
        $tpl = $pdf->importPage(1);
        $pdf->AddPage();
        $pdf->useTemplate($tpl);
        $pdf->SetFont('Helvetica', 'B');
        $pdf->SetFontSize(self::FONT_SIZE);
        $pdf->SetXY(self::POSITION_X, self::POSITION_Y); 
        $pdf->Cell(0, 10, $this->_name, 0, 0, 'C');
        $dateTime = new DateTime();
        $slug = strtolower(preg_replace('/[^a-z0-9]+/i', '_', $this->_name)) . '_';
        $slug .= $dateTime->format('Ymd_His') . '_' . rand(1000, 9999);
        $filename = 'wgeo_certificate_' . $slug . '.pdf';
        $pdf->Output($this->_certificatePath . '/' . $filename, 'F');
        
        return $filename;
    }
    
    /**
     * get error message
     * 
     * @return string
     */
    public function getError()
    {
        return $this->_error;
    }
    
    /**
     * check certificate save directory
     * 
     * @param int $sessionId
     */
    private function _checkCertificateDirectory($sessionId)
    {
        $path = UPLOAD_DIR . '/certificates';
        if (! is_dir($path)) {
            mkdir($path);
        }
        $sessionPath = $path . '/' . $sessionId;
        if (! is_dir($sessionPath)) {
            mkdir($sessionPath);
        }
        $this->_certificatePath = $sessionPath;
    }
    
    /**
     * load FPDI
     */
    private function _loadPdfLibrary()
    {
        define('FPDF_PATH', LIB_DIR . '/legacy/fpdf');
        define('FPDF_FONTPATH', FPDF_PATH . '/font/');
        include_once FPDF_PATH . '/fpdf.php';
        include_once FPDF_PATH . '/fpdi.php';
    }

}
