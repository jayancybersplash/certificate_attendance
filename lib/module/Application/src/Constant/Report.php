<?php
/**
 * Application Constant Report
 * 
 * @category Application
 * @package  Application\Constant
 */

namespace Application\Constant;


class Report
{
    const TYPE_ATTENDEE = 1;
    const TYPE_ATTENDANCE = 2;
    const MODE_VIEW = 1;
    const MODE_EXPORT = 2;
    const ATTENDANCE_ALL = 0;
    const ATTENDANCE_PRESENT = 1;
    const ATTENDANCE_ABSENT = 2;
    const ATTENDANCE_PRESENT_ANY = 3;
    const ATTENDANCE_ABSENT_ANY = 4;

    public function __construct() { }
    
}
