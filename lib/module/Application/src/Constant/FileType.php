<?php
/**
 * Application Constant Tab
 * 
 * @category Application
 * @package  Application\Constant
 */

namespace Application\Constant;


class FileType
{
    const JPEG = 1;
    const PNG = 2;
    const GIF = 3;
    const CSV = 4;
    const PDF = 5;
    const DOC = 6;
    const DOCX = 7;
    const XLS = 8;
    const XLSX = 9;
    const ZIP = 10;
    
    public function __construct() { }
    
}
