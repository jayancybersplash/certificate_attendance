<?php
/**
 * Application Constant UserType
 * 
 * @category Application
 * @package  Application\Constant
 */

namespace Application\Constant;


class UserType
{
    const ADMIN = 1;
    const MANAGER = 2;

    public function __construct() { }
    
}
