<?php
/**
 * Application Constant Tab
 * 
 * @category Application
 * @package  Application\Constant
 */

namespace Application\Constant;


class Tab
{
    const DASHBOARD = 1;
    const SESSION = 2;
    const REPORT = 3;
    const USER = 4;

    public function __construct() { }
    
}
