<?php
/**
 * Application Constant FlashMessage
 * 
 * @category Application
 * @package  Application\Constant
 */

namespace Application\Constant;


class FlashMessage
{
    const CREATE = 'create';
    const CHANGE = 'change';
    const REMOVE = 'remove';
    const NO_CHANGE = 'nochange';
    
    public function __construct() { }
    
}
