<?php
/**
 * Application Constant SessionConstant
 * 
 * @category Application
 * @package  Application\Constant
 */

namespace Application\Constant;


class SessionConstant
{
    const DATEPICKER_FORMAT_PHP = 'd/m/Y';
    const DATEPICKER_FORMAT_JAVASCRIPT = 'DD/MM/YYYY';
    const TAB_OVERVIEW = 0;
    const TAB_EDIT = 1;
    const TAB_MODULE = 2;
    const TAB_ATTENDEE = 3;
    const TAB_ATTENDANCE = 4;
    const TAB_TEMPLATE = 5;
    const TAB_LOCK = 6;
    
    const CERTIFICATE_GENERATE = 1;
    const CERTIFICATE_RE_GENERATE = 2;
    const CERTIFICATE_REMOVE = 3;
    
    public function __construct() { }
    
}
