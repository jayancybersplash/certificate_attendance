<?php
/**
 * Application Recover Password Form
 * 
 * @category Application
 * @package  Form
 */

namespace Application\Form;

use Web\Form\Form;
use Application\Model\LoginModel;
use Web\Security\Password;
use Web\Core\Session;
use Web\Form\Captcha;


class RecoverPasswordForm extends Form
{
    const MAX_ATTEMPTS = 3;
    const ATTEMPTS_SESSION = 'RecoverPasswordAttempts';
    
    /**
     *
     * @var int 
     */
    private $_loginCount;
    
    /**
     *
     * @var \Web\Form\Captcha 
     */
    private $_captcha;
    
    /**
     *
     * @var string 
     */
    private $_identityColumn = 'email';
        
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'credential3', 
        ];
        $this->addFields($fields);
        $this->_loginCount = Session::get(self::ATTEMPTS_SESSION, 0);
        $this->_captcha = new Captcha();
    }
    
    /**
     * get Captcha instance
     * 
     * @return \Web\Form\Captcha
     */
    public function captcha()
    {
        return $this->_captcha;
    }
    
    /**
     * validate form
     *
     * @param \Application\Model\LoginModel $model
     * @return boolean
     */
    public function isValid(LoginModel $model, $captchaEnabled = true)
    {
        $this->csrf();
        if ($this->_loginCount > self::MAX_ATTEMPTS) {
            die('Unauthorized access, retry limit exceeded');
        }
        if ($captchaEnabled) {
            if (! $this->_captcha->isValid()) {
                $this->error->invalid('Verification code');
            }
        }
        if ($this->noError()) {
            $this->postback();
            $username = $this->getValue('credential3');
            if ($this->validator->isNull($username)) {
                $this->error->required('Email');
            } else {
                if ($model->checkByColumn($this->_identityColumn, $username)) {
                    $user = $model->selectByColumn($this->_identityColumn, $username);
                    if (1 == $user->active) {
                        Session::clear(self::ATTEMPTS_SESSION);
                    } else {
                        $this->error->add('Invalid Email OR disabled account');
                    }
                } else {
                    $this->error->add('Invalid Email OR disabled account');
                }
            }
            if ($this->hasError()) {
                ++$this->_loginCount;
                Session::set(self::ATTEMPTS_SESSION, $this->_loginCount);
            }
        }
        
        return $this->noError();
    }
    
}
