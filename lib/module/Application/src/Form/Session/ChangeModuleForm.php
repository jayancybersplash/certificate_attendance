<?php
/**
 * Application Session Change Module Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\Session
 */

namespace Application\Form\Session;

use Web\Form\Form;

use Model\SessionModule;


class ChangeModuleForm extends Form
{   
    
    public function __construct()
    {
        parent::__construct();
        $fields = [ 'module' ];
        $this->addFields($fields);
        $this->multiselect('module');
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        $this->postback();
        $modules = $this->getValue('module');
        if (empty($modules)) {
            $this->error->notSelected('Modules');
        }
        
        return $this->noError();
    }
    
    /**
     * change record
     * 
     * @return int
     */
    public function change()
    {
        $sessionId = $this->getId();
        $sessionModuleModel = new SessionModule();
        $sql = $sessionModuleModel->getSql();
        $sql->whereId('session_id', $sessionId);
        $existing = $sessionModuleModel->selectPairs(['id', 'module_id'], $sql->getWhere());
        $selected = $this->getValue('module');
        $toDelete = array_diff($existing, $selected);
        $new = array_diff($selected, $existing);
        if (empty($toDelete) && empty($new)) {
            return false;
        }
        
        if (! empty($toDelete)) {
           $sessionModuleModel->deleteByIds(array_keys($toDelete)); 
        }
        if (! empty($new)) {
            $columns = ['session_id', 'module_id'];
            $insert = $sessionModuleModel->getInsertPreparedStatement($columns);
            foreach ($new as $moduleId) {
                $insert->execute([$sessionId, $moduleId]);
            }
        }
        
        return true;
    }
    
}
