<?php
/**
 * Application Session Create Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\Session
 */

namespace Application\Form\Session;

use Application\Form\Template;
use Application\Form\SessionValidator;
use Application\Utility\SessionDateHelper;

use Web\Form\Form;

use Model\SessionModule;


class CreateForm extends Form
{
    /**
     *
     * @var \Application\Form\Template
     */
    private $_template;
    
    /**
     *
     * @var \Application\Form\SessionValidator
     */
    private $_sessionValidator;
    
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'title',
            'start_date',
            'end_date',
            'location',
            'description',
            'module',
        ];
        $this->addFields($fields);
        $this->multiselect('module');
        $this->_template = new Template('template', 'Certificate template');
        $this->_sessionValidator = new SessionValidator();
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        $values = $this->postback();
        if ($this->validator->isNull($values['title'])) {
            $this->error->required('Title');
        }
        
        // check dates
        $this->_validateDate($values['start_date'], 'Start date');
        $this->_validateDate($values['end_date'], 'End date');
        
        if ($this->noError()) {
            if (! $this->_sessionValidator->period($values['start_date'], $values['end_date'])) {
                $this->error->add('Invalid date range');
            }
        }
        
        if ($this->validator->isNull($values['location'])) {
            $this->error->required('Location');
        }
        
        if (empty($values['module'])) {
            $this->error->notSelected('Modules');
        }
        
        // validate template file
        if (! $this->_template->isValid()) {
            $this->error->add($this->_template->getError());
        }
        
        if ($this->noError()) {
            $sql = $this->getModel()->getSql();
            $sql->where('title', $values['title'])
                ->where('location', $values['location'])
                ->where('start_date', SessionDateHelper::toSql($values['start_date']))
                ->where('end_date', SessionDateHelper::toSql($values['end_date']));
            if ($this->getModel()->selectTotal($sql->getWhere()) > 0) {
                $this->error->exists('Session');
            }
        }
        
        return $this->noError();
    }
    
    /**
     * create record
     * 
     * @return int
     */
    public function create()
    {
        $values = $this->getValues();
        $insert = [
            'title' => $values['title'],
            'location' => $values['location'],
            'description' => $values['description'],
            'start_date' => SessionDateHelper::toSql($values['start_date']),
        ];
        if ($values['end_date'] == $values['start_date']) {
            $insert['end_date'] = $insert['start_date'];
        } else {
            $insert['end_date'] = SessionDateHelper::toSql($values['end_date']);
        }
        
        $sessionId = $this->save($insert);
        $this->_saveModules($sessionId);
        $this->_uploadTemplate($sessionId);
        
        return $sessionId;
    }
    
    /**
     * save modules
     * 
     * @param int $sessionId
     */
    private function _saveModules($sessionId)
    {
        $modules = $this->getValue('module');
        $sessionModuleModel = new SessionModule();
        $columns = ['session_id', 'module_id'];
        $insert = $sessionModuleModel->getInsertPreparedStatement($columns);
        foreach ($modules as $moduleId) {
            $insert->execute([$sessionId, $moduleId]);
        }
    }
    
    /**
     * upload template file
     * 
     * @param int $sessionId
     */
    private function _uploadTemplate($sessionId)
    {
        $uploadPath = 'templates';
        if ($this->_template->hasFile()) {
            $this->_template->setUploadPath(UPLOAD_DIR);
            if ($this->_template->upload($uploadPath)) {
                $filename = $this->_template->getUploadedFilename();
                $this->getModel()->updateById(['certificate_template' => $filename], $sessionId);
            }
        }
    }
    
    /**
     * validate date
     * 
     * @param string $value
     * @param string $title
     */
    private function _validateDate($value, $title)
    {
        if ($this->validator->isNull($value)) {
            $this->error->required($title);
        } elseif (! $this->_sessionValidator->date($value)) {
            $this->error->invalid($title);
        }
    }
    
}
