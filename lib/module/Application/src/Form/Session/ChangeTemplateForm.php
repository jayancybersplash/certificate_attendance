<?php
/**
 * Application SessionTemplate Change Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\Session
 */

namespace Application\Form\Session;

use Application\Form\Template;
use Web\Form\Form;


class ChangeTemplateForm extends Form
{   
    /**
     *
     * @var \Application\Form\Template
     */
    private $_template;
    
    public function __construct()
    {
        parent::__construct();
        $this->addField('test');
        $this->_template = new Template('template', 'Certificate template');
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        if (! $this->_template->isValid(['required' => true])) {
            $this->error->add($this->_template->getError());
        }
        
        return $this->noError();
    }
    
    /**
     * upload template
     * 
     * @return boolean
     */
    public function change()
    {
        $uploadPath = 'templates';
        if ($this->_template->hasFile()) {
            $this->_template->setUploadPath(UPLOAD_DIR);
            if ($this->_template->upload($uploadPath)) {
                $filename = $this->_template->getUploadedFilename();
                return $this->save(['certificate_template' => $filename]);
            }
        }
        
        return false;
    }
    
}
