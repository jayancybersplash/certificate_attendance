<?php
/**
 * Application Session Change Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\Session
 */

namespace Application\Form\Session;

use Application\Form\SessionValidator;
use Application\Utility\SessionDateHelper;

use Web\Form\Form;


class ChangeForm extends Form
{
    /**
     *
     * @var \Application\Form\SessionValidator
     */
    private $_sessionValidator;
    
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'title',
            'start_date',
            'end_date',
            'location',
            'description'
        ];
        $this->addFields($fields);
        $this->trackChanges();
        $this->_sessionValidator = new SessionValidator();
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        $values = $this->postback();
        if ($this->validator->isNull($values['title'])) {
            $this->error->required('Title');
        }
        
        // check dates
        $this->_validateDate($values['start_date'], 'Start date');
        $this->_validateDate($values['end_date'], 'End date');
        
        if ($this->noError()) {
            if (! $this->_sessionValidator->period($values['start_date'], $values['end_date'])) {
                $this->error->add('Invalid date range');
            }
        }
        
        if ($this->validator->isNull($values['location'])) {
            $this->error->required('Location');
        }

        if ($this->noError()) {
            $sql = $this->getModel()->getSql();
            $sql->where('title', $values['title'])
                ->where('location', $values['location'])
                ->where('start_date', SessionDateHelper::toSql($values['start_date']))
                ->where('end_date', SessionDateHelper::toSql($values['end_date']))
                ->whereNotId('id', $this->getId());
            if ($this->getModel()->selectTotal($sql->getWhere()) > 0) {
                $this->error->exists('Session');
            }
        }
        
        return $this->noError();
    }
    
    /**
     * change record
     * 
     * @return int
     */
    public function change()
    {
        $modified = $this->getModified();
        if (empty($modified)) {
            return false;
        }
        
        $update = $modified;
        if (isset($modified['start_date'])) {
            $update['start_date'] = SessionDateHelper::toSql($modified['start_date']);
        }
        if (isset($modified['end_date'])) {
            $update['end_date'] = SessionDateHelper::toSql($modified['end_date']);
        }
        
        return $this->save($update);
    }
    
    /**
     * validate date
     * 
     * @param string $value
     * @param string $title
     */
    private function _validateDate($value, $title)
    {
        if ($this->validator->isNull($value)) {
            $this->error->required($title);
        } elseif (! $this->_sessionValidator->date($value)) {
            $this->error->invalid($title);
        }
    }
    
}
