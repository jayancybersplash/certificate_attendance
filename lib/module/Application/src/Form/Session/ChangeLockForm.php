<?php
/**
 * Application Session Change Lock Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\Session
 */

namespace Application\Form\Session;

use Web\Form\Form;


class ChangeLockForm extends Form
{   
    
    public function __construct()
    {
        parent::__construct();
        $this->addField('test');
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        $this->postback();
        
        return $this->noError();
    }
    
    /**
     * change record
     * 
     * @param int $locked
     * @return int
     */
    public function change($locked)
    {
        return $this->save(['locked' => $locked]);
    }
    
}
