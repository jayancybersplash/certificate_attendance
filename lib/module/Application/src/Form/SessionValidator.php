<?php
/**
 * Application Session Validator
 * 
 * @category Application
 * @package  Form
 */

namespace Application\Form;

use Application\Utility\SessionDateHelper;

use Web\Form\FormValidator;


class SessionValidator extends FormValidator
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * validate date
     * @param string $value
     * @return boolean
     */
    public function date($value)
    {
        if ($this->isNull($value)) {
            return false;
        }
        
        return SessionDateHelper::checkDatepicker($value);
    }
    
    /**
     * check date period
     * 
     * @param string $start start date
     * @param string $end end date
     * @return boolean
     */
    public function period($start, $end)
    {
        return SessionDateHelper::checkRange($start, $end, true);
    }
    
}
