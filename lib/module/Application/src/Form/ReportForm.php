<?php
/**
 * Application Report Form
 * 
 * @category   Application
 * @package    Application\Form
 */

namespace Application\Form;

use Web\Form\Form;
use Application\Constant\Report;
use Model\Attendee;
use Model\Attendance;


class ReportForm extends Form
{
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'session_id',
            'type',
            'mode',
            'module',
            'order',
            'attendance',
            'attendance_mark',
            'absent_mark',
            'attendance_mark_custom',
            'absent_mark_custom',
            'attendee_field',
            'sort',
        ];
        $this->addFields($fields);
        $this->multiselect(['module', 'attendee_field']);
    }
    
    /**
     * validate form
     * 
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        $values = $this->postback();
        if (! $this->validator->selected($values['session_id'])) {
            $this->error->notSelected('Session');
        }
        if (empty($values['attendee_field'])) {
            $this->error->notSelected('Attendee fields');
        }
        
        if (Report::TYPE_ATTENDANCE == $values['type']) {
            if (empty($values['module'])) {
                $this->error->notSelected('Modules');
            }
        }
        
        if ($this->noError()) {
            $attendeeModel = new Attendee();
            if ($attendeeModel->checkByColumnId('session_id', $values['session_id'])) {
                if (Report::TYPE_ATTENDANCE == $values['type']) {
                    $sql = $attendeeModel->getSql();
                    $sql->whereId('session_id', $values['session_id']);
                    $where = $sql->getWhere();
                    $attendeeIds = $attendeeModel->selectIds($where);
                    $sql->clear();
                    $sql->whereIn('attendee_id', $attendeeIds);
                    $attendanceModel = new Attendance();
                    if (0 == $attendanceModel->selectTotal($sql->getWhere())) {
                        $this->error->add('No attendance details available');
                    }
                }
            } else {
                $this->error->add('No attendees found');
            }
        }
        
        return $this->noError();
    }
    
}
