<?php
/**
 * Application Attendance Form
 * 
 * @category   Application
 * @package    Application\Form
 */

namespace Application\Form;

use Web\Form\Form;
use Model\Attendance;


class AttendanceForm extends Form
{   
    public function __construct()
    {
        parent::__construct();
        $this->addFields(['attendance']);
        $this->multiselect('attendance');
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid()
    {
        $this->csrf();
        $this->postback();
        
        return $this->noError();
    }
    
    /**
     * update attendance
     * 
     * @return boolean
     */
    public function change($sessionModuleId)
    {
        $changed = false;
        $attendanceModel = new Attendance();
        $sql = $attendanceModel->getSql();
        $sql->whereId('session_module_id', $sessionModuleId);
        $where = $sql->getWhere();
        $current = $attendanceModel->selectPairs(['id', 'attendee_id'], $where);
        $selected = $this->getValue('attendance');
        if (empty($selected)) {
            if (! empty($current)) {
                $attendanceModel->delete($where, count($current));
                $changed = true;
            }
        } else {
            if (empty($current)) {
                $changed = true;
                $insert = $attendanceModel->getInsertPreparedStatement(['session_module_id', 'attendee_id']);
                foreach ($selected as $attendeeId) {
                    $insert->execute([$sessionModuleId, $attendeeId]);
                }
            } else {
                $new = array_diff($selected, $current);
                if (! empty($new)) {
                    $changed = true;
                    $insert = $attendanceModel->getInsertPreparedStatement(['session_module_id', 'attendee_id']);
                    foreach ($new as $attendeeId) {
                        $insert->execute([$sessionModuleId, $attendeeId]);
                    }
                }
                
                $removed = array_diff($current, $selected);
                if (! empty($removed)) {
                    $changed = true;
                    $attendanceModel->deleteByIds(array_keys($removed));
                }
            }
        }
        
        return $changed;
    }
    
}
