<?php
/**
 * Form Template
 *
 * @category   Application
 * @package    Application\Form
 */

namespace Application\Form;

use Web\Form\FileUpload;


class Template extends FileUpload
{    
    public function __construct($field = null, $title = null)
    {
        parent::__construct($field, $title);
    }
    
    /**
     * validate upload
     * 
     * @param array $options
     * @return boolean
     */
    public function isValid($options = [])
    {
        $options['pdf'] = true;
        
        return $this->validate($options);
    }

}
