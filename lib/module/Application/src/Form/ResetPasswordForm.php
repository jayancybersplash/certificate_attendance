<?php
/**
 * ResetPassword Form
 * 
 * @category Application
 * @package  Form
 */

namespace Application\Form;

use Web\Form\Form;
use Web\Security\Password;
use Web\Core\Session;
use Web\Utility\DateHelper;


class ResetPasswordForm extends Form
{
    const MAX_ATTEMPTS = 3;
    const ATTEMPTS_SESSION = 'ResetAttempts';
    
    private $_attemptCount;
    private $_captcha;
        
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'resetemail',
            'resetpassword1',
            'resetpassword2'
        ];
        $this->addFields($fields);
        $this->_attemptCount = Session::get(self::ATTEMPTS_SESSION, 0);
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid($token, $model)
    {
        $this->csrf();
        if ($this->_attemptCount > self::MAX_ATTEMPTS) {
            die('Unauthorized access, Retry limit exceeded');
        }
        
        $this->postback();
        $values = $this->getValues();
        if ($this->validator->isNull($values['resetemail'])) {
            $this->error->required('Email');
        } elseif (! $this->validator->email($values['resetemail'])) {
            $this->error->invalid('Email');
        }
        if ($this->validator->isNull($values['resetpassword1'])) {
            $this->error->required('New Password');
        } elseif ($this->validator->isNull($values['resetpassword2'])) {
            $this->error->required('Repeat New Password');
        } elseif ($values['resetpassword1'] != $values['resetpassword2']) {
            $this->error->noMatch('New Password', 'Repeat New Password');
        } elseif (! $this->validator->password($values['resetpassword1'])) {
            $this->error->noFormat('New Password');
        }
        if ($this->noError()) {
            $request = $model->getVerificationRequest($token);
            if ($values['resetemail'] != $request->email) {
                $this->error->invalid('Credentials');
            }
        }
        if ($this->hasError()) {
            ++$this->_attemptCount;
            Session::set(self::ATTEMPTS_SESSION, $this->_attemptCount);
        }

        return $this->noError();
    }
    
}
