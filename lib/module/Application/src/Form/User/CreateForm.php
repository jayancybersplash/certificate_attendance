<?php
/**
 * Application User Create Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\User
 */

namespace Application\Form\User;

use Web\Form\Form;
use Web\Security\Password;


class CreateForm extends Form
{   
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'email', 
            'first_name',
            'last_name',
            'user_password1',
            'user_password2',
            'active',
            'user_type_id',
        ];
        $this->addFields($fields);
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid($userTypesAllowed)
    {
        $this->csrf();
        $this->postback();
        $values = $this->getValues();
        if ($this->validator->isNull($values['first_name'])) {
            $this->error->required('first_name');
        }
        if ($this->validator->isNull($values['last_name'])) {
            $this->error->required('last_name');
        }
        if ($this->validator->isNull($values['email'])) {
            $this->error->required('Email');
        } elseif (! $this->validator->email($values['email'])) {
            $this->error->invalid('Email');
        } elseif ($this->getModel()->checkByColumn('email', $values['email'])) {
            $this->error->exists('Email');
        }
        if ($this->validator->selected($values['user_type_id'])) {
            if (! in_array($values['user_type_id'], $userTypesAllowed)) {
                $this->error->add('Invalid User Type selection');
            }
        } else {
            $this->error->notSelected('User Type');
        }
        
        if ($this->validator->isNull($values['user_password1'])) {
            $this->error->required('Password');
        } elseif ($this->validator->isNull($values['user_password2'])) {
            $this->error->required('Repeat Password');
        } elseif ($values['user_password1'] != $values['user_password2']) {
            $this->error->noMatch('Password', 'Repeat Password');
        } elseif (! $this->validator->password($values['user_password1'])) {
            $this->error->noFormat('Password');
        }
        
        return $this->noError();
    }
    
    /**
     * create record
     * 
     * @return int
     */
    public function create()
    {
        $data = $this->getValues();
        $data['access_key'] = Password::encrypt($data['user_password1']);
        unset($data['user_password1']);
        unset($data['user_password2']);
        
        return $this->save($data);
    }
    
}
