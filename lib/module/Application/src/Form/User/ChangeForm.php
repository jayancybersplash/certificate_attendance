<?php
/**
 * Application User Change Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\User
 */

namespace Application\Form\User;

use Application\Constant\UserType;

use Web\Form\Form;
use Web\Security\Password;


class ChangeForm extends Form
{   
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'email', 
            'first_name',
            'last_name',
            'user_type_id',
            'active',
            'user_password3',
            'user_password4',
        ];
        $this->addFields($fields);
        $this->trackChanges();
    }
    
    /**
     * validate form
     *
     * @return boolean
     */
    public function isValid($userTypesAllowed)
    {
        $this->csrf();
        $this->postback();
        $values = $this->getValues();
        if ($this->validator->isNull($values['first_name'])) {
            $this->error->required('first_name');
        }
        if ($this->validator->isNull($values['last_name'])) {
            $this->error->required('last_name');
        }
        if ($this->validator->isNull($values['email'])) {
            $this->error->required('Email');
        } elseif (! $this->validator->email($values['email'])) {
            $this->error->invalid('Email');
        } elseif ($this->getModel()->checkByColumnExceptId('email', $values['email'], $this->getId())) {
            $this->error->exists('Email');
        }
        if ($this->validator->selected($values['user_type_id'])) {
            if (! in_array($values['user_type_id'], $userTypesAllowed)) {
                $this->error->add('Invalid User Type selection');
            }
        } else {
            $this->error->notSelected('User Type');
        }
        if ($this->validator->notNull($values['user_password3']) || 
                $this->validator->notNull($values['user_password4'])) {
            if ($this->validator->isNull($values['user_password3'])) {
                $this->error->enter('New Password');
            } elseif ($this->validator->isNull($values['user_password4'])) {
                $this->error->required('Repeat New Password');
            } elseif ($values['user_password3'] != $values['user_password4']) {
                $this->error->noMatch('New Password', 'Repeat New Password');
            } elseif (! $this->validator->password($values['user_password3'])) {
                $this->error->noFormat('New Password');
            }
        }

        return $this->noError();
    }

    /**
     * change record
     * 
     * @return boolean
     */
    public function change()
    {
        $data = $this->getModified();
        if (! empty($data)) {
             if (isset($data['user_password3'])) {
                $data['access_key'] = Password::encrypt($data['user_password3']);
                unset($data['user_password3']);
                unset($data['user_password4']);
            }
            
            return $this->save($data);
        }
        
        return false;
    }
    
}
