<?php
/**
 * Application Login Form
 * 
 * @category Application
 * @package  Form
 */

namespace Application\Form;

use Model\User;

use Web\Form\Form;
use Web\Security\Password;
use Web\Core\Session;
use Web\Form\Captcha;


class LoginForm extends Form
{
    const MAX_ATTEMPTS = 3;
    const ATTEMPTS_SESSION = 'LoginAttempts';
    
    /**
     *
     * @var int 
     */
    private $_loginCount;
    
    /**
     *
     * @var \Web\Form\Captcha 
     */
    private $_captcha;
    
    /**
     *
     * @var string 
     */
    private $_identityColumn = 'email';
    
    /**
     *
     * @var string 
     */
    private $_passwordColumn = 'access_key';
        
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'credential1', 
            'credential2',
        ];
        $this->addFields($fields);
        $this->_loginCount = Session::get(self::ATTEMPTS_SESSION, 0);
        $this->_captcha = new Captcha();
    }
    
    /**
     * get Captcha instance
     * 
     * @return \Web\Form\Captcha
     */
    public function captcha()
    {
        return $this->_captcha;
    }
    
    /**
     * validate form
     *
     * @param \Model\User $model
     * @return boolean
     */
    public function isValid(User $model, $captchaEnabled = true)
    {
        $this->csrf();
        if ($this->_loginCount > self::MAX_ATTEMPTS) {
            die('Unauthorized access, retry limit exceeded');
        }
        if ($captchaEnabled) {
            if (! $this->_captcha->isValid()) {
                $this->error->invalid('Verification code');
            }
        }
        if ($this->noError()) {
            $this->postback();
            $username = $this->getValue('credential1');
            $password = $this->getValue('credential2');
            if ($this->validator->isNull($username)) {
                $this->error->required('Username / Password');
            } elseif ($this->validator->isNull($password)) {
                $this->error->required('Username / Password');
            } else {
                if ($model->checkByColumn($this->_identityColumn, $username)) {
                    $user = $model->selectByColumn($this->_identityColumn, $username);
                    if (! Password::match($password, $user->{$this->_passwordColumn})) {
                        $this->error->invalid('Username / Password');
                    }
                } else {
                    $this->error->invalid('Username / Password');
                }
            }
            if ($this->hasError()) {
                ++$this->_loginCount;
                Session::set(self::ATTEMPTS_SESSION, $this->_loginCount);
            }
        }
        
        return $this->noError();
    }
    
}
