<?php
/**
 * Application Profile Form
 * 
 * @category Application
 * @package  Application\Form
 */

namespace Application\Form;

use Web\Form\Form;
use Web\Security\Password;


class ProfileForm extends Form
{   
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'first_name',
            'last_name',
            'email',
            'profile_password1', 
            'profile_password2',
            'profile_password3',
        ];
        $this->addFields($fields);
    }
    
    /**
     * validate form
     * 
     * @param \Model\User $model
     * @param int $id
     *
     * @return boolean
     */
    public function isValid($model, $id)
    {
        $this->csrf();
        $this->postback();
        $values = $this->getValues();
        if ($this->validator->isNull($values['first_name'])) {
            $this->error->required('first_name');
        }
        if ($this->validator->isNull($values['last_name'])) {
            $this->error->required('last_name');
        }
        if ($this->validator->isNull($values['email'])) {
            $this->error->required('Email');
        } elseif (! $this->validator->email($values['email'])) {
            $this->error->invalid('Email');
        }
        $passwordChange = false;
        if ($this->validator->notNull($values['profile_password2'])
            || $this->validator->notNull($values['profile_password3'])) {
            $passwordChange = true;
            if ($this->validator->isNull($values['profile_password1'])) {
                $this->error->required('Current Password');
            }
            if ($this->validator->isNull($values['profile_password2'])) {
                $this->error->required('New Password');
            } elseif ($this->validator->isNull($values['profile_password3'])) {
                $this->error->required('Repeat New Password');
            } elseif ($values['profile_password2'] != $values['profile_password3']) {
                $this->error->noMatch('New Password', 'Repeat New Password');
            } elseif (! $this->validator->password($values['profile_password2'])) {
                $this->error->noFormat('New Password');
            }
        }
        
        if ($this->noError()) {
            if ($model->checkByColumnExceptId('email', $values['email'], $id)) {
                $this->error->add('Email already used');
            }
            if ($passwordChange) {
                $credential = $model->selectColumnById('access_key', $id);
                if (! Password::match($values['profile_password1'], $credential)) {
                    $this->error->incorrect('Current Password');
                }
            }
        }

        return $this->noError();
    }

}
