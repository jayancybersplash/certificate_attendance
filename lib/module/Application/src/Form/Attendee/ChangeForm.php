<?php
/**
 * Application Change Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\Attendee
 */

namespace Application\Form\Attendee;

use Web\Form\Form;

use Application\Utility\NameOnCertificate;
use Model\Attendee;


class ChangeForm extends Form
{   
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'first_name',
            'last_name',
            'name_on_certificate',
            'email',
            'company',
            'designation',
        ];
        $this->addFields($fields);
        $this->trackChanges();
    }
    
    /**
     * validate form
     *
     * @param int $sessionId
     * @return boolean
     */
    public function isValid($sessionId)
    {
        $this->csrf();
        $values = $this->postback();

        if ($this->validator->isNull($values['first_name'])) {
            $this->error->required('First name');
        }
        
        if ($this->validator->isNull($values['last_name'])) {
            $this->error->required('Last name');
        }
        
        if ($this->validator->isNull($values['email'])) {
            $this->error->required('Email');
        } elseif (! $this->validator->email($values['email'])) {
            $this->error->invalid('Email');
        } else {
            $sql = $this->getModel()->getSql();
            $where = $sql->whereId('session_id', $sessionId)
                         ->whereEqual('email', $values['email'])
                         ->whereNotId('id', $this->getId())
                         ->getWhere();
            $attendeeModel = new Attendee();
            if ($attendeeModel->selectTotal($where) > 0) {
                $this->error->add('Email already exists in the session');
            }
        }
        
        if ($this->validator->isNull($values['company'])) {
            $this->error->required('Company');
        }
        if ($this->validator->isNull($values['designation'])) {
            $this->error->required('Designation');
        }
        
        return $this->noError();
    }

    /**
     * change record
     * 
     * @return boolean
     */
    public function change()
    {
        $data = $this->getModified();
        if (empty($data)) {
            return false;
        }
        
        if ('' == trim($this->getValue('name_on_certificate'))) {
            $helper = new NameOnCertificate($this->getValue('first_name'), $this->getValue('last_name'));
            $data['name_on_certificate'] = $helper->get();
        }
        
        return $this->save($data);
    }
    
}
