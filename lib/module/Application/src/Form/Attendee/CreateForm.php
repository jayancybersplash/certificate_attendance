<?php
/**
 * Application Attendee Create Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\Attendee
 */

namespace Application\Form\Attendee;

use Web\Form\Form;
use Web\Utility\DateHelper;

use Application\Utility\NameOnCertificate;
use Model\Attendee;


class CreateForm extends Form
{   
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'first_name',
            'last_name',
            'name_on_certificate',
            'email',
            'company',
            'designation',
        ];
        $this->addFields($fields);
    }
    
    /**
     * validate form
     *
     * @param int $sessionId
     * @return boolean
     */
    public function isValid($sessionId)
    {
        $this->csrf();
        $values = $this->postback();

        if ($this->validator->isNull($values['first_name'])) {
            $this->error->required('First name');
        }
        
        if ($this->validator->isNull($values['last_name'])) {
            $this->error->required('Last name');
        }
        
        if ($this->validator->isNull($values['email'])) {
            $this->error->required('Email');
        } elseif (! $this->validator->email($values['email'])) {
            $this->error->invalid('Email');
        } else {
            $sql = $this->getModel()->getSql();
            $where = $sql->whereId('session_id', $sessionId)
                         ->whereEqual('email', $values['email'])
                         ->getWhere();
            $attendeeModel = new Attendee();
            if ($attendeeModel->selectTotal($where) > 0) {
                $this->error->add('Email already added to the session');
            }
        }
        
        if ($this->validator->isNull($values['company'])) {
            $this->error->required('Company');
        }
        if ($this->validator->isNull($values['designation'])) {
            $this->error->required('Designation');
        }
        
        return $this->noError();
    }
    
    /**
     * create record
     * 
     * @param int $userId
     * @param int $sessionId
     * @return int
     */
    public function create($userId, $sessionId)
    {
        $data = $this->getValues();
        if ('' == trim($data['name_on_certificate'])) {
            $helper = new NameOnCertificate();
            $data['name_on_certificate'] = $helper->setFirstName($data['first_name'])
                                                  ->setLastName($data['last_name'])
                                                  ->get();
        }
        $data['session_id'] = $sessionId;
        $data['created_on'] = DateHelper::now();
        $data['created_by'] = $userId; 
        
        return $this->save($data);
    }
    
}
