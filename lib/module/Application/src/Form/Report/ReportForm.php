<?php
/**
 * Application Report Form
 * 
 * @category   Application
 * @package    Application\Form
 * @subpackage Application\Form\User
 */

namespace Application\Form\Report;

use Web\Form\Form;


class ReportForm extends Form
{   
    public function __construct()
    {
        parent::__construct();
        $fields = [
            'session', 
            'module',
        ];
        $this->addFields($fields);
        $this->postback();
    }
    
}
