<?php
/**
 * Application Main Controller
 * 
 * @category Application
 */

namespace Application;

use Web\Mvc\ActionController;
use Web\Core\Registry;
use Web\Core\Server;
use Web\Database\Table\Db;
use Web\Utility\ActionLinkHelper;
use Application\Utility\Paging;
use Application\Utility\Audit;
//use Application\Constant\UserType;
use Application\Constant\SessionConstant;


class ApplicationController extends ActionController
{
    /**
     *
     * @var int '
     */
    protected $_userType = 0;
    
    /**
     *
     * @var boolean 
     */
    private $_auditEnabled = false;
    
    public function __construct()
    {
        $module = Registry::get('module');
        parent::__construct($module['session'], Db::USER);
        $this->plugin('head')->setTitlePrefix(Registry::get('siteName') . ' :: ');
        $this->setHeadTitle('Home');
        $this->layout->setLayout('index');
        $this->view->setVariable('showFormFlash', true);
        $this->plugin('script')->addModuleBody('common');
        $this->plugin('tab')->set(1);
        $this->setTheme('chameleon');
        if ($this->session->isLogged()) {
            $this->_userType = $this->session->get('user_type_id');
        }
        $this->view->setVariable('datepickerDateFormat', SessionConstant::DATEPICKER_FORMAT_JAVASCRIPT);
    }
    
    /**
     * get current user id
     * 
     * @return int
     */
    public function getUserId()
    {
        return $this->session->getId();
    }
    
    /**
     * get user type
     * 
     * @return int
     */
    public function getUserType()
    {
        return $this->_userType;
    }

    /**
     * load action link helper
     */
    public function loadActionLink()
    {
        ActionLinkHelper::run();
    }
    
    /**
     * check if super admin
     * 
     * @return boolean
     */
    public function superAdmin()
    {
        return 1 == $this->session->getId();
    }
    
    /**
     * Generate Paging
     * 
     * @param int $total 
     * @return StdClass 
     */
    protected function getPaging($total = 0, $limit = null, $generateLinks = true)
    {
        $paging = new Paging();
        $result = $paging->run($total, $limit, $generateLinks);
        $this->view->setVariable('paging', $result->html);
        $this->view->setVariable('pagingInfo', $result->info);
        $this->view->counter($result->sqlOffset);
        
        return $result;
    }
    
    /**
     * Action link cancel redirect
     */
    protected function cancelRedirect()
    {
        $this->redirect(ActionLinkHelper::getRedirectLink());
    }
    
    /**
     * save audit log
     * 
     * @param string $action
     * @param string $description
     * @param string $request
     * @return int
     */
    protected function auditLog($section, $action, $description = null, $request = null)
    {
        if (! $this->_auditEnabled) {
            return false;
        }
        
        $audit = new Audit();
        $audit->setId($this->getUserId())
              ->setSection($section)
              ->setAction($action)
              ->setDescription($description)
              ->setRequest($request);
        return $audit->save();
    }
    
    /**
     * check user types allowed
     * 
     * @param string|array $userTypes
     * @return boolean
     */
    protected function allowed($userTypes)
    {
        if (is_array($userTypes)) {
            if (in_array($this->_userType, $userTypes)) {
                return true;
            }
        } else {
            if ($this->_userType == $userTypes) {
                return true;
            }
        }
        $this->accessRestricted();
    }
    
    /**
     * check access declined for user types
     * 
     * @param string|array $userTypes
     * @return boolean
     */
    protected function declined($userTypes)
    {
        if (is_array($userTypes)) {
            if (! in_array($this->_userType, $userTypes)) {
                return true;
            }
        } else {
            if ($this->_userType != $userTypes) {
                return true;
            }
        }
        $this->accessRestricted('/');
    }
    
    /**
     * add data tables scripts
     */
    protected function addDatatableScripts()
    {
        $scripts = $this->plugin('script');
        $scripts->addThemeBody('app-assets/vendors/js/tables/datatable/datatables.min.js');
        //$scripts->addThemeBody('app-assets/js/scripts/tables/datatables/datatable-basic.js');
        $scripts->addThemeBody('assets/js/datatable-basic.js');
    }
    
    /**
     * check delete request by controller name
     * 
     * @param string $controller
     * @return boolean
     */
    protected function checkDeleteRequest($controller)
    {
        $valid = false;
        $referer = Server::referer();
        if (! empty($referer)) {
            if (false === strpos($referer, Server::requestUri())) {
                $valid = 0 === strpos($referer, $this->registry('siteUrl') . $controller);
            }
        }
        if (! $valid) {
            $this->accessRestricted($controller);
        }
        
        return true;
    }
    
    /**
     * access restricted message
     * 
     * @param string $url
     */
    protected function accessRestricted($url = '/')
    {
        $this->setFlashMessage('Access restricted', $url, 'error');
    }
    
    /**
     * add datepicker scripts
     */
    protected function addDatepicker()
    {
        $scripts = $this->plugin('script');
        $this->plugin('script')->addThemeBody('app-assets/vendors/js/pickers/dateTime/moment-with-locales.min.js');
        $this->plugin('script')->addThemeBody('app-assets/vendors/js/pickers/daterange/daterangepicker.js');
    }
    
    /**
     * load notifications
     */
    protected function loadNotifications()
    {
//        if (! $this->session->isLogged()) {
//            return false;
//        }
//        $userTypes = [
//            UserType::ADMIN, 
//            UserType::MANAGER,
//        ];
//        if (! in_array($this->_userType, $userTypes)) {
//            $this->view->setVariable('showNotifications', false);
//            return true;
//        }
//        
//        $notificationModel = new NotificationModel();
//        $notifications = [
//            'total' => 0,
//            'unread' => 0,
//            'list' => [],
//        ];
//        $userId = $this->getUserId();
       // if ($notificationModel->checkUserNotifications($userId)) {
//            $notifications['total'] = $notificationModel->getUserNotificationCount($userId);
//            $notifications['unread'] = $notificationModel->getUserUnreadNotificationCount($userId);
//            $notifications['list'] = $notificationModel->getLatestNotifications($userId, 5);
//            $this->view->setVariable('showNotifications', true);
//            $this->view->setVariable('notifications', $notifications);
       // }
    }
    
}
