<?php
/**
 * Application Profile Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;
use Application\Form\ProfileForm;
use Web\Security\Password;

use Model\User;


class ProfileController extends ApplicationController
{
    /**
     *
     * @var \Model\User 
     */
    private $_model;
    
    public function __construct()
    {
        parent::__construct();
        $this->loginRequired();
        $this->_model = new User();
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction()
    {
        $id = $this->getUserId();
        $form = new ProfileForm();
        if ($form->isPost()) {
            if ($form->isValid($this->_model, $id)) {
                $values = $form->getValues();
                $profile = $this->_model->selectById($id);
                $update = [];
                if ($values['first_name'] != $profile->first_name) {
                    $update['first_name'] = $values['first_name'];
                }
                if ($values['last_name'] != $profile->last_name) {
                    $update['last_name'] = $values['last_name'];
                }
                if ($values['email'] != $profile->email) {
                    $update['email'] = $values['email'];
                }
                if ('' != $values['profile_password2']) {
                    $update['access_key'] = Password::encrypt($values['profile_password2']);
                }
                if (! empty($update)) {
                    $this->_model->updateById($update, $id);
                    $this->setFlashMessage('Profile updated');
                } else {
                    $this->setFlashMessage('No changes made to Profile');
                }
                $this->redirect('profile');
            }
        } else {
            $profile = $this->_model->selectById($id);
            $populate = [
                'first_name' => $profile->first_name,
                'last_name' => $profile->last_name,
                'email' => $profile->email,
            ];
            $form->populate($populate);
        }
        
        $this->registerForm($form);
        $this->setHeadTitle('Profile');
        $this->setPageHeading('Profile');
        $breadrumb = $this->plugin('breadcrumb');
        $breadrumb->add('Profile');
        
        //$this->loadNotifications();

        return $this->getViewModel();
    }

}
