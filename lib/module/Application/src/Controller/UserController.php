<?php
/**
 * Application User Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;
use Application\Form\User\CreateForm;
use Application\Form\User\ChangeForm;
use Application\Constant\Tab;
use Application\Constant\FlashMessage;
use Application\Constant\UserType as UserTypeConstant;

use Web\View\Support\LookupStore;

use Model\User;
use Model\UserType;


class UserController extends ApplicationController
{
    /**
     *
     * @var \Model\User 
     */
    private $_model;
    
    public function __construct()
    {
        parent::__construct();
        $this->loginRequired();
        $this->_adminOnly();
        
        $this->_model = new User();
        $this->currentTab(Tab::USER);
        $this->loadActionLink();
        $this->setFlashMessageTitle('User');
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction()
    {
        $this->view->setVariable('currentUserId', $this->getUserId());
        $sql = $this->_model->getSql();
        $sql->whereNotId('id', 1);
        if (! $this->superAdmin()) {
            $sql->whereNotId('user_type_id', UserTypeConstant::ADMIN);
        }
        $where = $sql->getWhere();
        $total = $this->_model->selectTotal($where); 
        if ($total > 0) {
            $this->view->setVariable('list', $this->_model->select($where, 'first_name'));
        }
        
        $this->view->setVariable('total', $total);

        LookupStore::register('user_type', $this->_getUserTypes());
        LookupStore::register('status', ['Disabled', 'Active']);
        
        $this->setHeadTitle('Users');
        $this->_breadcrumb('List');
        $this->addDatatableScripts();
        
        return $this->getViewModel();
    }
   
    /**
     * create action
     * 
     * @return \Web\View\ViewModel
     */
    public function createAction()
    {
        $userTypesAllowed = $this->_getUserTypesAllowed();
        if (empty($userTypesAllowed)) {
            $this->accessRestricted('user');
        }
        
        $form = new CreateForm();
        $form->setModel($this->_model);
        if ($form->isPost()) {
            if ($form->isValid(array_keys($userTypesAllowed))) {
                $form->create();
                $this->setActionFlashMessage(FlashMessage::CREATE);
                $this->redirect('user');
            }
        } else {
            $userTypeIds = array_keys($userTypesAllowed);
            $populate = [
                'active' => 1,
                'user_type_id' => $userTypeIds[0],
            ];
            $form->populate($populate);
        }
        
        $this->registerForm($form);
        LookupStore::register('user_type', $userTypesAllowed);
        LookupStore::register('status', ['Disabled', 'Active']);
            
        $this->setHeadTitle('Users :: New');
        $this->setPageHeading('Users');
        $this->setPageSubheading('New');
        $this->_breadcrumb('New');
        
        return $this->getViewModel();
    }
    
    /**
     * change action
     * 
     * @return \Web\View\ViewModel
     */
    public function changeAction()
    {
        $id = $this->_getId();
        
        $this->_checkChangeDeleteAccess($id);
        $form = new ChangeForm();
        $form->setModel($this->_model);
        $form->setId($id);
        $userTypesAllowed = $this->_getUserTypesAllowed();
        
        if ($form->isPost()) {
            if ($form->isValid(array_keys($userTypesAllowed))) {
                if ($form->change()) {
                    $this->setActionFlashMessage(FlashMessage::CHANGE);
                } else {
                   $this->setActionFlashMessage(FlashMessage::NO_CHANGE); 
                }
                $this->_cancel();
            }
        } else {
            $user = $this->_model->selectById($id);
            $populate = [
                'email' => $user->email, 
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'user_type_id' => $user->user_type_id,
                'active' => $user->active,
            ];
            $form->populate($populate);
        }
        
        LookupStore::register('user_type', $userTypesAllowed);
        LookupStore::register('status', ['Disabled', 'Active']);
        
        $this->registerForm($form);
        $this->setHeadTitle('Users :: Edit');
        $this->setPageHeading('Users');
        $this->setPageSubheading('Edit');
        $this->_breadcrumb('Edit');
        
        return $this->getViewModel();
    }
    
    /**
     * remove action
     * 
     * @return \Web\View\ViewModel
     */
    public function removeAction()
    {
        $this->checkDeleteRequest('user');
        $id = $this->_getId();
        $this->_checkChangeDeleteAccess($id);
        $this->_model->deleteById($id);
        $this->setActionFlashMessage(FlashMessage::REMOVE);
        $this->_cancel();
        
        return $this->getViewModel();
    }

    /**
     * set page breadcrumb
     * 
     * @param string $page
     */
    private function _breadcrumb($page = null)
    {
        $breadrumb = $this->plugin('breadcrumb');
        $breadrumb->add('Users', $this->view->actionUrl('user'));
        if (null !== $page) {
            $breadrumb->add($page);
        }
    }
    
    /**
     * Action link cancel redirect
     */
    private function _cancel()
    {
        $this->redirect(ActionLinkHelper::getRedirectLink());
    }
  
    /**
     * get id
     * 
     * @return int
     */
    private function _getId()
    {
        $valid = false;
        $id = $this->getParam('id', true);
        if ($id > 0) {
            $valid = $this->_model->hasId($id);
        }
        if (! $valid) {
            $this->setFlashMessage('Invalid request', 'user', 'error');
        }
        
        return $id;
    }
    
    /**
     * get user types
     * 
     * @return array
     */
    private function _getUserTypes()
    {
        $userTypeModel = new UserType();
        
        return $userTypeModel->selectPairs(['id', 'type']);
    }
    
    /**
     * check access for change and delete
     * 
     * @param int $id
     * @return boolean
     */
    private function _checkChangeDeleteAccess($id)
    {
        if ($id < 2 || $id == $this->getUserId()) {
            $this->setFlashMessage('Invalid request', 'user', 'error');
        }
        if ($this->superAdmin()) {
            return true;
        }
        
        $user = $this->_model->selectById($id);
        switch ($this->_userType) {
            case UserTypeConstant::ADMIN:
                $valid = $user->user_type_id != UserTypeConstant::ADMIN;
                break;

            default:
                $valid = false;
                break;
        }
        if (! $valid) {
            $this->setFlashMessage('Access restricted', 'user', 'error');
        }
    }
    
    /**
     * get user types allowed
     * 
     * @return array
     */
    private function _getUserTypesAllowed()
    {
        $userTypes = $this->_getUserTypes();
        if ($this->superAdmin()) {
            $userTypesAllowed = $userTypes;
        } else {
            switch ($this->_userType) {
                case UserTypeConstant::ADMIN:
                    $userTypesAllowed = $userTypes;
                    unset($userTypesAllowed[UserTypeConstant::ADMIN]);
                    break;
                
                default:
                    $userTypesAllowed = [];
                    break;
            }
        }
        
        return $userTypesAllowed;
    }
    
    /**
     * check if admin 
     * 
     */
    private function _adminOnly()
    {
        if (UserTypeConstant::ADMIN != $this->getUserType()) {
            $this->accessRestricted();
        }
    }
    
}
