<?php

/**
 * Application Attendance Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;
use Application\Form\AttendanceForm;
use Application\Constant\Tab;
use Application\Constant\FlashMessage;
use Application\Constant\SessionConstant;

use Model\SessionModule;
use Model\Module;
use Model\Session;
use Model\Attendee;
use Model\Attendance;


class AttendanceController extends ApplicationController
{

    /**
     *
     * @var \Model\Session 
     */
    private $_model;

    /**
     *
     * @var int 
     */
    private $_id;
    
    /**
     *
     * @var \stdClass
     */
    private $_session;

    public function __construct() 
    {
        parent::__construct();
        $this->loginRequired();

        $this->_model = new Session();
        $this->setHeadTitle('Sessions');
        $this->setPageSubheading('Attendance');
        $this->currentTab(Tab::SESSION);
        $this->setFlashMessageTitle('Attendance');
    }

    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction() 
    {
        $this->_loadSession();
        
        $moduleModel = new Module();
        $moduleCodes = $moduleModel->selectPairs(['id', 'code']);
        
        $sessionModuleModel = new SessionModule();
        $sql = $this->_model->getSql();
        $sql->whereId('session_id', $this->_id);
        $whereSession = $sql->getWhere();
        $sessionModules = $sessionModuleModel->selectPairs(['id', 'module_id'], $whereSession);
        $modules = [];
        $attendanceDefault = [];
        foreach ($moduleCodes as $moduleId => $code) {
            if (in_array($moduleId, $sessionModules)) {
                $sessionModuleId = array_search($moduleId, $sessionModules);
                $modules[$sessionModuleId] = $code;
                $attendanceDefault[$sessionModuleId] = 0;
            }
        }
        
        $attendees = [];
        $attendance = [];
        $attendeeModel = new Attendee();
        if ($attendeeModel->selectTotal($whereSession) > 0) {
            $attendees = $attendeeModel->select($whereSession, 'first_name');
            foreach ($attendees as $attendee) {
                $attendance[$attendee->id] = $attendanceDefault;
            }
            
            $attendanceModel = new Attendance();
            $sql->clear();
            $sql->whereIn('session_module_id', array_keys($modules));
            $whereSessionModules = $sql->getWhere();
            if ($attendanceModel->selectTotal($whereSessionModules) > 0) {
                $sessionAtttendance = $attendanceModel->select($whereSessionModules);
                foreach ($sessionAtttendance as $item) {
                    if (isset($attendance[$item->attendee_id])) {
                        $attendance[$item->attendee_id][$item->session_module_id] = 1;
                    }
                }
            }
        }

        $this->view->setVariable('attendees', $attendees);
        $this->view->setVariable('attendance', $attendance);
        $this->view->setVariable('modules', $modules);
        $this->_breadcrumb('Attendance');
        $this->_setTab();

        return $this->getViewModel();
    }

    /**
     * change action
     * 
     * @return \Web\View\ViewModel
     */
    public function changeAction() 
    {
        $this->_loadSession();
        $this->_checkLock();
        $sessionModuleId = $this->_getSessionModuleId();
        $attendeeModel = new Attendee();
        $sql = $this->_model->getSql();
        $sql->where('session_id', $this->_id);
        $whereSession = $sql->getWhere();
        if (0 == $attendeeModel->selectTotal($whereSession)) {
            $this->setFlashMessage('No attendees added', 'attendance/index/id/' . $this->_id, 'error');
        }
        
        $form = new AttendanceForm();
        if ($form->isPost()) {
            if ($form->isValid()) {
                try {
                    $this->_model->getAdapter()->beginTransaction();
                    if ($form->change($sessionModuleId)) {
                        $this->setActionFlashMessage(FlashMessage::CHANGE);
                    } else {
                        $this->setActionFlashMessage(FlashMessage::NO_CHANGE);
                    }
                    $this->_model->getAdapter()->commit();
                    $this->_goToList();
                } catch (\PDOException $ex) {
                    //echo $ex->getMessage(); exit;
                    $this->_model->getAdapter()->rollBack();
                    $this->setFlashMessage('Unable to update attendance', 'attendance/index/id/' . $this->_id, 'error');
                }
            }
        } else {
            $attendance = [];
            $sql->clear();
            $where = $sql->where('session_module_id', $sessionModuleId)
                         ->getWhere();
            $attendanceModel = new Attendance();
            if ($attendanceModel->selectTotal($where) > 0) {
                $attendance = $attendanceModel->selectColumns('attendee_id', $where);
            }
            $form->populate(['attendance' => $attendance]);
        }

        $this->view->setVariable('attendees', $attendeeModel->select($whereSession, 'first_name'));
        
        $sessionModuleModel = new SessionModule();
        $moduleId = $sessionModuleModel->selectColumnById('module_id', $sessionModuleId);
        $moduleModel = new Module();
        $this->view->setVariable('module', $moduleModel->find($moduleId));

        $this->registerForm($form);
        $this->_breadcrumb('Edit Attendance');
        $this->_setTab();

        return $this->getViewModel();
    }
    
    /**
     * set page breadcrumb
     * 
     * @param string $page
     */
    private function _breadcrumb($page = null) 
    {
        $breadrumb = $this->plugin('breadcrumb');
        $breadrumb->add('Sessions', $this->view->actionUrl('session'));
        if (null !== $page) {
            $breadrumb->add($page);
        }

    }

    /**
     * get id
     * 
     * @return int
     */
    private function _getId() 
    {
        $valid = false;
        $id = $this->getParam('id', true);
        if ($id > 0) {
            $valid = $this->_model->hasId($id);
        }
        if (! $valid) {
            $this->setFlashMessage('Invalid request', 'session', 'error');
        }
        $this->_id = $id;

        return $id;
    }

    /**
     * get session module id
     * 
     * @return int
     */
    private function _getSessionModuleId()
    {
        $valid = false;
        $id = $this->getParam('module', true);
        if ($id > 0) {
            $sessionModuleModel = new SessionModule();
            if ($sessionModuleModel->hasId($id)) {
                $valid = $this->_id == $sessionModuleModel->selectColumnById('session_id', $id);
            }
        }
        if (! $valid) {
            $this->setFlashMessage('Invalid request', 'session/attendance/id/' . $this->_id, 'error');
        }

        return $id;
    }
    
    /**
     * load session
     */
    private function _loadSession()
    {
        $this->_getId();
        $this->_session = $this->_model->find($this->_id);
    }
    
    /**
     * set tab and view variables
     */
    private function _setTab()
    {
        $this->view->setVariable('sessionId', $this->_id);
        $this->view->setVariable('session', $this->_session);
        $this->view->setVariable('tab', SessionConstant::TAB_ATTENDANCE);
    }
    
    /**
     * check session lock
     */
    private function _checkLock()
    {
        if ($this->_session->locked > 0) {
            $this->setFlashMessage('Session locked', 'attendance/index/id/' . $this->_id, 'error');
        }
    }
    
    /**
     * go to attendance view
     */
    private function _goToList()
    {
        $this->redirect('attendance/index/id/' . $this->_id);
    }

}
