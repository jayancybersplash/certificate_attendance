<?php
/**
 * Application Session Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;
use Application\Constant\Tab;
use Application\Constant\SessionConstant;
use Application\Constant\FlashMessage;
use Application\Constant\UserType as UserTypeConstant;

use Application\Form\Session\CreateForm;
use Application\Form\Session\ChangeForm;
use Application\Form\Session\ChangeModuleForm;
use Application\Form\Session\ChangeTemplateForm;
use Application\Form\Session\ChangeLockForm;

use Application\Utility\SessionDateHelper;

use Web\View\Support\LookupStore;

use Model\Session;
use Model\Module;
use Model\SessionModule;
use Model\Attendee;


class SessionController extends ApplicationController
{
    /**
     *
     * @var /Model/Session 
     */
    private $_model;
    
    /**
     *
     * @var int 
     */
    private $_id;
    
    /**
     *
     * @var boolean 
     */
    private $_isAdmin = false;
    
    /**
     *
     * @var \stdClass
     */
    private $_session;
    
    public function __construct()
    {
        parent::__construct();
        $this->loginRequired();
        
        $this->_model = new Session();
        $this->setHeadTitle('Sessions');
        $this->currentTab(Tab::SESSION);
        $this->setFlashMessageTitle('Session');
        $this->_isAdmin = UserTypeConstant::ADMIN == $this->getUserType();
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction()
    {
        $total = $this->_model->selectTotal(); 
        if ($total > 0) {
            $this->view->setVariable('list', $this->_model->select(null, 'end_date DESC'));
            $attendeeModel = new Attendee();
            LookupStore::register('attendees', $attendeeModel->selectGroupCount('session_id'));
        }
        $this->view->setVariable('statusOptions', ['Open', 'Locked']);
        $this->view->setVariable('statusColorOptions', ['success', 'danger']);
        
        $this->_breadcrumb('List');
        $this->view->setVariable('total', $total);
        $actionAccess = [];
        if ($this->superAdmin()) {
            $actionAccess[] = 'delete';
        }
        $this->view->setVariable('actionAccess', $actionAccess);
        $this->addDatatableScripts();
        
        return $this->getViewModel();
    }
    
    /**
     * create action
     * 
     * @return \Web\View\ViewModel
     */
    public function createAction()
    {
        $this->_adminOnly();
        $form = new CreateForm();
        $form->setModel($this->_model);
        $module = new Module();
        if ($form->isPost()) {
            if ($form->isValid()) {
                try {
                    $this->_model->getAdapter()->beginTransaction();
                    $form->create();
                    $this->_model->getAdapter()->commit();
                    $this->setActionFlashMessage(FlashMessage::CREATE);
                    $this->redirect('session');
                } catch (\PDOException $ex) {
                    //echo $ex->getMessage(); exit;
                    $this->_model->getAdapter()->rollBack();
                    $this->setFlashMessage('Unable to create session', 'session/create', 'error');
                }
            }
        } else {
            $form->populate(['module' => $module->selectColumns('id')]);
        }
        
        $this->registerForm($form);
        $this->addDatepicker();
        $this->plugin('script')->addModuleBody('session-create');
        $this->view->setVariable('modules', $module->select());
        $this->setHeadTitle('Sessions :: New');
        $this->setPageHeading('Sessions');
        $this->setPageSubheading('New');
        $this->_breadcrumb('New');
        
        return $this->getViewModel();
    }
    
    /**
     * Change action
     * 
     * @return \Web\View\ViewModel
     */
    public function changeAction()
    {
        $this->_getId();
        $tab = $this->getParam('tab', true);
        $this->_session = $this->_model->selectById($this->_id);
        switch ($tab) {
            case SessionConstant::TAB_EDIT:
                $this->_editDetails();
                break;
            
            case SessionConstant::TAB_MODULE:
                $this->_editModule();
                break;
            
            case SessionConstant::TAB_TEMPLATE:
                $this->_editTemplate();
                break;
            
            case SessionConstant::TAB_LOCK:
                $this->_editLock();
                break;
            
            default:
                $this->_overview();
                break;
        }

        $this->setHeadTitle('Sessions :: Edit');
        $this->setPageHeading('Sessions');
        $this->setPageSubheading('Edit');
        
        $this->view->setVariable('tabUrl', $this->view->actionUrl('session/change/id/' . $this->_id));
        $this->view->setVariable('tab', $tab);
        $this->view->setVariable('sessionId', $this->_id);
        $this->view->setVariable('session', $this->_session);
        $this->view->setVariable('editPermission', $this->_isAdmin);
        
        return $this->getViewModel();
    }
    
    /**
     * remove action
     * 
     * @return \Web\View\ViewModel
     */
    public function removeAction() 
    {
        $this->_getId();
        $session = $this->_model->find($this->_id);
        if (0 == $session->locked) {
            $this->checkDeleteRequest('session');
            $this->_model->deleteById($this->_id);
            $this->setActionFlashMessage(FlashMessage::REMOVE);
        }
        
        $this->redirect('session');
        
        return $this->getViewModel();
    }
    
    
    /**
     * get id
     * 
     * @return int
     */
    private function _getId()
    {
        $valid = false;
        $id = $this->getParam('id', true);
        if ($id > 0) {
            $valid = $this->_model->hasId($id);
        }
        if (! $valid) {
            $this->setFlashMessage('Invalid request', 'session', 'error');
        }
        $this->_id = $id;
        
        return $id;
    }
    
    /**
     * edit session details
     * 
     */
    private function _editDetails()
    {
        $form = new ChangeForm();
        $form->setModel($this->_model);
        $form->setId($this->_id);
        $populateForm = true;
        if ($this->_isAdmin) {
            if ($form->isPost()) {
                if ($form->isValid()) {
                    if ($form->change()) {
                        $this->setActionFlashMessage(FlashMessage::CHANGE);
                    } else {
                        $this->setActionFlashMessage(FlashMessage::NO_CHANGE);
                    }
                    $this->_reload();
                }
                $populateForm = false;
            }
        }
        
        if ($populateForm) {
            $populate = [
                'title' => $this->_session->title,
                'start_date' => SessionDateHelper::view($this->_session->start_date),
                'end_date' => SessionDateHelper::view($this->_session->end_date),
                'location' => $this->_session->location,
                'description' => $this->_session->description
            ];
            $form->populate($populate);
        }
        
        $this->registerForm($form);
        $this->addDatepicker();
        $this->plugin('script')->addModuleBody('session-change');
        $this->_setEditTab(SessionConstant::TAB_EDIT);
        $this->_breadcrumb('Edit');
    }
    
    /**
     * session overview tab
     * 
     */
    private function _overview()
    {
        $moduleModel = new Module();
        $this->view->setVariable('modules', $moduleModel->select());
        
        $sessionModuleModel = new SessionModule();
        $sql = $this->_model->getSql();
        $sql->whereId('session_id', $this->_id);
        $this->view->setVariable('sessionModules', $sessionModuleModel->selectColumns('module_id', $sql->getWhere()));
        
        $attendeeModel = new Attendee();
        $this->view->setVariable('attendees', $attendeeModel->selectTotalByIdColumn('session_id', $this->_id));

        $this->_setEditTab(SessionConstant::TAB_OVERVIEW);
        $this->layout->setScript('session/change_overview');
        $this->_breadcrumb('Edit');
    }
    
    /**
     * set edit tab
     * 
     * @param int $tab
     */
    private function _setEditTab($tab)
    {
        $this->view->setVariable('tab', $tab);
    }
    
    /**
     * edit modules
     * 
     */
    private function _editModule()
    {
        $form = new ChangeModuleForm();
        $form->setModel($this->_model);
        $form->setId($this->_id);
        if ($form->isPost()) {
            if ($form->isValid()) {
                try {
                    $this->_model->getAdapter()->beginTransaction();
                    $this->setFlashMessageTitle('Modules');
                    if ($form->change()) {
                        $this->setActionFlashMessage(FlashMessage::CHANGE);
                    } else {
                        $this->setActionFlashMessage(FlashMessage::NO_CHANGE);
                    }
                    $this->_model->getAdapter()->commit();
                    $this->_reload();
                } catch (\PDOException $ex) {
                    //echo $ex->getMessage(); exit;
                    $this->_model->getAdapter()->rollBack();
                    $this->setFlashMessage('Unable to update modules', 'session/change/id/' . $this->_id . '/tab/' . SessionConstant::TAB_MODULE, 'error');
                }
            }
        } else {
            $sessionModuleModel = new SessionModule();
            $sql = $this->_model->getSql();
            $sql->whereId('session_id', $this->_id);
            $selectedModuleIds = $sessionModuleModel->selectColumns('module_id', $sql->getWhere());
            $form->populate(['module' => $selectedModuleIds]);
        }
        $this->registerForm($form);
        $moduleModel = new Module();
        $this->view->setVariable('modules', $moduleModel->select());
        $this->layout->setScript('session/change_module');
        $this->_setEditTab(SessionConstant::TAB_MODULE);
        $this->plugin('script')->addModuleBody('session-change-module');
        $this->_breadcrumb('Edit Modules');
    }
    
    /**
     * edit certificate template
     * 
     */
    private function _editTemplate()
    {
        $form = new ChangeTemplateForm();
        $form->setModel($this->_model);
        $form->setId($this->_id);
        if ($form->isPost()) {
            if ($form->isValid()) {
                $this->setFlashMessageTitle('Certificate template');
                if ($form->change()) {
                    $this->setActionFlashMessage(FlashMessage::CHANGE);
                } else {
                    $this->setActionFlashMessage(FlashMessage::NO_CHANGE);
                }
                $this->_reload();
            }
        }
        
        $this->registerForm($form);
        
        $this->layout->setScript('session/change_template');
        $this->_setEditTab(SessionConstant::TAB_TEMPLATE);
        $this->plugin('script')->addModuleBody('session-change-template');
        $this->_breadcrumb('Edit Certificate Template');
    }
    
    /**
     * edit lock
     * 
     */
    private function _editLock()
    {
        $form = new ChangeLockForm();
        $form->setModel($this->_model);
        $form->setId($this->_id);
        if ($this->_isAdmin) {
            if ($form->isPost()) {
                if ($form->isValid()) {
                    if ($this->_session->locked > 0) {
                        $locked = 0;
                    } else {
                        $locked = 1;
                    }
                    $form->change($locked);                    
                    $this->setFlashMessageTitle('Lock status');
                    $this->setActionFlashMessage(FlashMessage::CHANGE);
                    $this->_reload();
                }
            }
        }
        
        $this->registerForm($form);
        $this->layout->setScript('session/change_lock');
        $this->_setEditTab(SessionConstant::TAB_LOCK);
        $this->_breadcrumb('Edit Lock');
        $this->plugin('script')->addModuleBody('session-change-lock');
    }
    
    /**
     * set page breadcrumb
     * 
     * @param string $page
     */
    private function _breadcrumb($page = null)
    {
        $breadrumb = $this->plugin('breadcrumb');
        $breadrumb->add('Sessions', $this->view->actionUrl('session'));
        if (null !== $page) {
            $breadrumb->add($page);
        }
    }
    
    /**
     * admin only
     * 
     * @return boolean
     */
    private function _adminOnly()
    {
        if (! $this->_isAdmin) {
            $this->accessRestricted('session');
        }
    }
    
    /**
     * reload page
     */
    private function _reload()
    {
        $this->plugin('redirect')->reload();
    }

}
