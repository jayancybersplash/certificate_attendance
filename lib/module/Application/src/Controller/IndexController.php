<?php
/**
 * Application Index Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;

use Web\View\Support\LookupStore;

use Model\Session;
use Model\Attendee;


class IndexController extends ApplicationController
{
    public function __construct()
    {
        parent::__construct();
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction()
    {
        if (! $this->session->isLogged()) {
            $this->_showLogin();
            
            return $this->getViewModel();
        }
        
        $model = new Session();
        $total = $model->selectTotal();
        if ($total > 0) {
            $attendeeModel = new Attendee();
            LookupStore::register('attendees', $attendeeModel->selectGroupCount('session_id'));
            LookupStore::register('status', ['Open', 'Locked']);
            LookupStore::register('statusColor', ['success', 'danger']);
            $this->view->setVariable('list', $model->select(null, 'end_date DESC'));
        }
        
        $this->view->setVariable('total', $total);
        $this->setHeadTitle('Home');
        
        return $this->getViewModel();
    }
    
    /**
     * logout action
     * 
     * @return \Web\View\ViewModel
     */
    public function logoutAction()
    {
        if ($this->session->isLogged()) {
            $this->session->logout();
        }
        $this->redirect('login');
        
        return $this->getViewModel();
    }
    
    /**
     * error action
     * 
     * @return \Web\View\ViewModel
     */
    public function errorAction()
    {
        if ($this->session->isLogged()) {
            $this->layout->setLayout('index');
        } else {
            $this->layout->setLayout('auth');
        }
        $this->setHeadTitle('Error');
        
        return $this->getViewModel();
    }
    
    /**
     * show login on home url
     */
    private function _showLogin()
    {
        $form = new \Application\Form\LoginForm();
        $this->view->setVariable('showPreLoader', true);
        $this->setHeadTitle('Login');
        $this->registerForm($form);
        $this->layout->setLayout('auth');
        $this->layout->setScript('login/index');
        $this->view->setVariable('captchaEnabled', true);
        $this->view->setVariable('showForgotPassword', false);
    }

}
