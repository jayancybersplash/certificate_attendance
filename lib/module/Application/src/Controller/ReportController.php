<?php
/**
 * Application Report Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;
use Application\Constant\Tab;
use Application\Form\ReportForm;
use Application\Constant\Report;
use Application\Form\AjaxForm;

use Web\Utility\DateHelper;
use Web\Utility\CsvWriter;

use Model\Session;
use Model\SessionModule;
use Model\Module;
use Model\Attendee;
use Model\Attendance;
 

class ReportController extends ApplicationController
{
    /**
     *
     * @var \Model\Session 
     */
    private $_model;
    
    /**
     * 
     * @var array
     */
    private $_attendanceMarkOptions;
    
    /**
     * 
     * @var array
     */
    private $_absentMarkOptions;
    
    /**
     * 
     * @var array
     */
    private $_formValues;
    
    /**
     * 
     * @var array
     */
    private $_options;
    
    public function __construct()
    {
        parent::__construct();
        $this->loginRequired();
        
        $this->setHeadTitle('Reports');
        $this->currentTab(Tab::REPORT);
         
        $this->_model = new Session();
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction()
    {
        if (0 == $this->_model->selectTotal()) {
            $this->layout->setScript('report/no-session');
            
            return $this->getViewModel();
        }
        
        $this->_loadMarkingOptions();
        
        $form = new ReportForm();
        if ($form->isPost()) {
            if ($form->isValid()) {
                $this->_formValues = $form->getValues();
                $this->_loadReportOptions();
                if (Report::MODE_EXPORT == $this->_formValues['mode']) {
                    $this->_export();
                }
                
                $this->_display();
                
                return $this->getViewModel();
            }
        } else {
            $default = [
                'type' => Report::TYPE_ATTENDEE,
                'mode' => Report::MODE_VIEW,
                'module' => [],
                'order' => 0,
                'attendance' => Report::ATTENDANCE_ALL,
                'attendance_mark' => 0,
                'absent_mark' => 0,
                'attendee_field' => [0, 1, 2, 3, 4, 5],
                'sort' => 0,
            ];
            $form->populate($default);
        }
        
        $sessionModules = [];
        $sessionOptions = [];
        $sessions = $this->_model->select(null, 'start_date DESC');
        foreach ($sessions as $item) {
            $sessionOptions[$item->id] = DateHelper::format('F Y', $item->start_date) . ', ' . $item->title;
        }
        
        $types = [
            Report::TYPE_ATTENDEE => 'Attendees',
            Report::TYPE_ATTENDANCE => 'Attendance',
        ];
        
        $modes = [
            Report::MODE_VIEW => 'View',
            Report::MODE_EXPORT => 'Export',
        ];
        
        $orderOptions = [
            'First name',
            'Last name',
            'Email',
            'ID',
        ];
        
        $attendanceOptions = [
            Report::ATTENDANCE_ALL => 'Include all',
            Report::ATTENDANCE_PRESENT => 'Attended all selected modules',
            Report::ATTENDANCE_ABSENT => 'Absent on all selected modules',
            Report::ATTENDANCE_PRESENT_ANY => 'Attended any of the selected modules',
            Report::ATTENDANCE_ABSENT_ANY => 'Absent on any of the selected modules',
            
        ];
        
        $attendeeFieldOptions = [
            'ID',
            'First name',
            'Last name',
            'Email',
            'Company',
            'Designation',
        ];
        
        
        $moduleModel = new Module();
        $this->view->setVariable('modules', $moduleModel->select());
        
        $sessionId = $form->getValue('session_id');
        if ($sessionId > 0) {
            $sessionModuleModel = new SessionModule();
            if ($sessionModuleModel->checkByColumnId('session_id', $sessionId)) {
                $sql = $this->_model->getSql();
                $sql->whereId('session_id', $sessionId);
                $sessionModules = $sessionModuleModel->selectColumns('module_id', $sql->getWhere());
            }
        }
        $this->view->setVariable('sessionModules', $sessionModules);
        
        $this->registerForm($form);
        $this->view->setVariable('sessionOptions', $sessionOptions);
        $this->view->setVariable('reportTypes', $types);
        $this->view->setVariable('reportModes', $modes);
        $this->view->setVariable('orderOptions', $orderOptions);
        $this->view->setVariable('attendanceOptions', $attendanceOptions);
        $this->view->setVariable('attendeeFieldOptions', $attendeeFieldOptions);
        $this->view->setVariable('attendanceMarkOptions', $this->_attendanceMarkOptions);
        $this->view->setVariable('absentMarkOptions', $this->_absentMarkOptions);
        $this->view->setVariable('sortOptions', ['Ascending', 'Descending']);
        
        $this->plugin('script')->addModuleBody('report');
        $this->_breadcrumb();

        return $this->getViewModel();
    }
    
    /**
     * get session modules action
     * 
     * @return \Web\View\ViewModel
     */
    public function getSessionModulesAction()
    {
        if ($this->isAjaxRequest()) {
            $response = $this->getAjaxResponse();
            $message = 'Invalid input';
            $form = new AjaxForm();
            if ($form->isPost()) {
                $values = $form->run();
                $id = (int) $values['id'];
                if ($id > 0) {
                    if ($this->_model->hasId($id)) {
                        $sessionModuleModel = new SessionModule();
                        if ($sessionModuleModel->checkByColumnId('session_id', $id)) {
                            $sql = $this->_model->getSql();
                            $sql->whereId('session_id', $id);
                            $modules = $sessionModuleModel->selectColumns('module_id', $sql->getWhere());
                            $response->addData($modules, 'modules');
                            $response->valid();
                            $message = 'success';
                        } else {
                            $message = 'No modules selected for the session';
                        }
                    }
                }
            }
            
            $response->message($message);
        }
        
        return $this->getViewModel();
    }
    
    /**
     * load attendance marking options
     */
    private function _loadMarkingOptions()
    {
        $this->_attendanceMarkOptions = [ 'Y', 'P', 'Yes', 'Present', 'Attended', '1', '',];
        $this->_absentMarkOptions = [ '', 'N', 'A', 'No', 'Absent', 'Not attended', '0', ];
    }
    
    /**
     * display report
     */
    private function _display()
    {
        if (Report::TYPE_ATTENDANCE == $this->_formValues['type']) {
            $this->_loadAttendance();
            
            $this->layout->setScript('report/attendance');
            $this->_breadcrumb('Attendance');
        } else {
            $this->layout->setScript('report/attendee');
            $this->_breadcrumb('Attendees');
        }
        $this->view->setVariable('options', $this->_options);
    }
    
    /**
     * attendance display
     */
    private function _loadAttendance()
    {
        $modules = [];
        $attendanceDefault = [];
        foreach ($this->_options['modules'] as $moduleId => $code) {
            if (in_array($moduleId, $this->_options['sessionModules'])) {
                if (in_array($moduleId, $this->_formValues['module'])) {
                    $sessionModuleId = array_search($moduleId, $this->_options['sessionModules']);
                    $modules[$sessionModuleId] = $code;
                    $attendanceDefault[$sessionModuleId] = 0;
                }
            }
        }
        $this->_options['selectedModules'] = $modules;
        $attendees = [];
        $attendance = [];
        foreach ($this->_options['attendees'] as $attendee) {
            $attendees[$attendee->id] = $attendee;
            $attendance[$attendee->id] = $attendanceDefault;
        }
        $attendanceModel  = new Attendance();
        $sql = $this->_model->getSql();
        $sql->whereIn('session_module_id', array_keys($modules));
        $attendanceList = $attendanceModel->select($sql->getWhere());
        foreach ($attendanceList as $item) {
            if (isset($attendance[$item->attendee_id])) {
                $attendance[$item->attendee_id][$item->session_module_id] = 1;
            }
        }
        
        switch ($this->_formValues['attendance']) {
            case Report::ATTENDANCE_PRESENT:
                foreach ($attendance as $attendeeId => $data) {
                    if (false !== array_search(0, $data)) {
                        unset($attendees[$attendeeId]);
                    }
                }
                break;
            
            case Report::ATTENDANCE_ABSENT:
                foreach ($attendance as $attendeeId => $data) {
                    if (false !== array_search(1, $data)) {
                        unset($attendees[$attendeeId]);
                    }
                }
                break;
                
            case Report::ATTENDANCE_PRESENT_ANY:
                foreach ($attendance as $attendeeId => $data) {
                    if (false === array_search(1, $data)) {
                        unset($attendees[$attendeeId]);
                    }
                }
                break;
                
            case Report::ATTENDANCE_ABSENT_ANY:
                foreach ($attendance as $attendeeId => $data) {
                    if (false === array_search(0, $data)) {
                        unset($attendees[$attendeeId]);
                    }
                }
                break;
            
            default:
                break;
        }
        $this->_options['indexedAttendees'] = $attendees;
        $this->_options['attendance'] = $attendance;
        $attendanceSummary = [];
        $moduleCount = count($modules);
        foreach ($attendance as $key => $value) {
            $item = array_count_values($value);
            if (isset($item[1])) {
                $attendanceSummary[$key] = $item[1] . ' / ' . $moduleCount;
            } else {
                $attendanceSummary[$key] = '0 / ' . $moduleCount;
            }
        }
        $marks = [
            $this->_absentMarkOptions[$this->_formValues['absent_mark']],
            $this->_attendanceMarkOptions[$this->_formValues['attendance_mark']],
        ];
        if ('' != $this->_formValues['absent_mark_custom']) {
            $marks[0] = $this->_formValues['absent_mark_custom'];
        }
        if ('' != $this->_formValues['attendance_mark_custom']) {
            $marks[1] = $this->_formValues['attendance_mark_custom'];
        }
        $this->_options['attendanceMarks'] = $marks;
        $this->_options['attendanceSummary'] = $attendanceSummary;
    }
    
    /**
     * export mode
     */
    private function _export()
    {
        if (Report::TYPE_ATTENDANCE == $this->_formValues['type']) {
            $this->_exportAttendance();
        } else {
            $this->_exportAttendees();
        }
    }
    
    /**
     * export attendees
     */
    private function _exportAttendees()
    {
        $filename = $this->_options['session']->title . ' Attendees'; 
        $csv = new CsvWriter($filename);
        $csv->row($this->_options['fields']);
        $fields = array_keys($this->_options['fields']);
        foreach ($this->_options['attendees'] as $attendee) {
            $row = [];
            foreach ($fields as $field) {
                $row[] = $attendee->{$field};
            }
            $csv->row($row);
        }
        $csv->finish();
    }
    
    /**
     * export attendance
     */
    private function _exportAttendance()
    {
        $this->_loadAttendance();
        $filename = $this->_options['session']->title . ' Attendance'; 
        $csv = new CsvWriter($filename);
        $titles = $this->_options['fields'] + $this->_options['selectedModules'];
        $titles[] = 'Summary';
        
        $csv->row($titles);
        $fields = array_keys($this->_options['fields']);
        foreach ($this->_options['indexedAttendees'] as $attendeeId => $attendee) {
            $row = [];
            foreach ($fields as $field) {
                $row[] = $attendee->{$field};
            }
            foreach ($this->_options['attendance'][$attendeeId] as $item) {
                $row[] = $this->_options['attendanceMarks'][$item];
            }
            $row[] = $this->_options['attendanceSummary'][$attendeeId];
            $csv->row($row);
        }
        
        $csv->finish();
    }
    
    /**
     * load report options
     */
    private function _loadReportOptions()
    {
        $this->_options = [];
        $this->_options['session'] = $this->_model->selectById($this->_formValues['session_id']);
        $fieldsMap = [
            'id' => 'ID',
            'first_name' =>  'First name',
            'last_name' =>  'Last name',
            'email' =>  'Email',
            'company' =>  'Company',
            'designation' =>  'Designation',
        ];
        $this->_options['fields'] = [];
        $key = 0;
        foreach ($fieldsMap as $column => $title) {
            if (in_array($key, $this->_formValues['attendee_field'])) {
                $this->_options['fields'][$column] = $title;
            }
            ++$key;
        }
        
        $orderFields = [ 'first_name', 'last_name', 'email', 'id', ];
        $this->_options['order'] = $orderFields[$this->_formValues['order']];
        if ($this->_formValues['sort'] > 0) {
            $this->_options['order'] .= ' DESC';
        }
        $attendeeModel = new Attendee();
        $sql = $this->_model->getSql();
        $sql->whereId('session_id', $this->_formValues['session_id']);
        $this->_options['attendees'] = $attendeeModel->select($sql->getWhere(), $this->_options['order']);
        if (Report::TYPE_ATTENDANCE == $this->_formValues['type']) {
            $moduleModel = new Module();
            $this->_options['modules'] = $moduleModel->selectPairs(['id', 'code']);
            $sessionModuleModel = new SessionModule();
            if ($sessionModuleModel->checkByColumnId('session_id', $this->_formValues['session_id'])) {
                $sql = $this->_model->getSql();
                $sql->whereId('session_id', $this->_formValues['session_id']);
                $this->_options['sessionModules'] = $sessionModuleModel->selectPairs(['id', 'module_id'], $sql->getWhere());
            }
        }
    }
    
    /**
     * set page breadcrumb
     * 
     * @param string $page
     */
    private function _breadcrumb($page = null)
    {
        $breadrumb = $this->plugin('breadcrumb');
        $breadrumb->add('Reports', $this->view->actionUrl('session'));
        if (null !== $page) {
            $breadrumb->add($page);
        }
    }
    
}
