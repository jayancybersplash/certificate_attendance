<?php
/** 
 * Application Login Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;
use Application\Form\LoginForm;
use Model\User;

use Web\Utility\DateHelper;
use Web\Core\Server;

/**
use Application\Form\RecoverPasswordForm;
use Application\Model\PasswordResetModel;
use Application\Form\ResetPasswordForm;
 * 
 */
use Web\Security\Password;


class LoginController extends ApplicationController
{
    private $_model;
    private $_keepLogged = false;
    private $_captchaEnabled = true;
    
    public function __construct()
    {
        parent::__construct();
        $this->_checkBlocked();
        if ($this->session->isLogged()) {
            $this->redirect();
        }
        $this->_model = new User();
        //echo Password::encrypt('123456'); exit;
        
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction()
    {
        
        $form = new LoginForm();
        if ($form->isPost()) {
            if ($form->isValid($this->_model, $this->_captchaEnabled)) {
                $username = $form->getValue('credential1');
                $user = $this->_model->selectByColumn('email', $username);
                $this->session->set($user->id);
                /*
                $login = [
                    'user_id' => $user->id,
                    'login_time' => DateHelper::timestamp(),
                    'remote_address' => Server::remoteAddress(),
                ];
                $this->_model->addLogin($login);
                if ($this->_keepLogged) {
                    $this->session->keepLogged($user->id, $user->login);
                }
                $description =  'User (' . $user->id . ') "' . $user->email . '" logged IN';
                $this->auditLog(AuditSection::NO_AUTH, AuditAction::LOGIN, $description);
                 * 
                 */
                $this->redirect();
            }
        } else {
            $this->view->setVariable('showPreLoader', true);
        }
        $this->setHeadTitle('Login');
        $this->registerForm($form);
        $this->layout->setLayout('auth');
        $this->view->setVariable('captchaEnabled', $this->_captchaEnabled);
        $this->view->setVariable('showForgotPassword', false);
        
        return $this->getViewModel();
    }
    
    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function recoverPasswordAction()
    {
        if ($this->session->isLogged()) {
            $this->redirect();
        }
        $form = new RecoverPasswordForm();
        if ($form->isPost()) {
            
            if ($form->isValid($this->_model, $this->_captchaEnabled)) {
                $email = $form->getValue('credential3');
                $user = $this->_model->selectByColumn('email', $email);
                $remoteAddress = Server::remoteAddress();
                $tokenBase = serialize($user) . $email . $remoteAddress 
                           . Server::userAgent() . microtime();
                $token = md5($tokenBase);
                $now = DateHelper::getDateTime();
                $request = [
                    'user_id' => $user->id,
                    'email' => $email,
                    'token' => $token,
                    'created_on' => $now->getTimestamp(),
                    'remote_address' => Server::remoteAddress(),
                    'used' => 0,
                ];
                
                // set expiry
                $now->modify('+10 minutes');
                $request['expiry'] = $now->getTimestamp();
                $model = new PasswordResetModel();
                $model->addRequest($request);
                $this->_sendConfirmationEmail($email, $token);
                $description = 'User (' . $user->id . ') "' . $user->email . '" requested password reset';
                $this->auditLog(AuditSection::NO_AUTH, AuditAction::PASSWORD_RESET_REQUEST, $description);
                $this->redirect();
                
            }
        }
        $this->setHeadTitle('Recover Password');
        $this->registerForm($form);
        $this->layout->setLayout('auth');
        $this->view->setVariable('captchaEnabled', $this->_captchaEnabled);
        
        return $this->getViewModel();
    }
    
    /**
     * reset action
     * 
     * @return \Web\View\ViewModel
     */
    public function resetAction()
    {
        $valid = false;
        $form = new ResetPasswordForm();
        $model = new PasswordResetModel();
        $verification = $this->getParam('verification');
        if ('' != $verification) {
            if ($this->_checkVerificationFormat($verification)) {
                if ($model->checkVerificationRequest($verification)) {
                    $request = $model->getVerificationRequest($verification);
                    if (0 == $request->used) {
                        if ($this->_checkExpiry($request->expiry)) {
                            if ($this->_model->hasId($request->user_id)) {
                                $user = $this->_model->selectById($request->user_id);
                                $valid = 1 == $user->active && 0 == strcasecmp($user->email,  $request->email);
                            }
                        }
                    }
                }
            }
        }
        
        if ($valid) {
            if ($form->isPost()) {
                if ($form->isValid($verification, $model)) {
                    $newPassword = $form->getValue('resetpassword1');
                    $update = [
                        'credential' => Password::encrypt($newPassword),
                    ];
                    $this->_model->updateById($update, $user->id);
                    $used = ['used' => 1, 'used_on' => DateHelper::timestamp()];
                    $model->updateVerificationRequest($used, $request->id);
                    $description = 'Password has been reset for User (' . $user->id . ') "' . $user->email . '"';
                    $this->auditLog(AuditSection::NO_AUTH, AuditAction::PASSWORD_RESET, $description);
                    $this->setFlashMessage('Your password has been updated. Please login to continue.');
                    $this->redirect('login');
                }
            }
        } else {
            $form->addError('Invalid or expired request');
        }

        $this->registerForm($form);
        $this->view->setVariable('validRequest', $valid);
        $this->setHeadTitle('Reset Password');
        $this->layout->setLayout('auth');
        
        return $this->getViewModel();
    }
    
    /**
     * send confirmation mail
     * 
     * @param string $email
     * @param string $token
     */
    private function _sendConfirmationEmail($email, $token)
    {
        $mailer = new Mailer();
        $mailer->setTemplate(Template::USER);
        $link = $this->registry('siteUrl') . 'login/reset/verification/' . $token;
        $message = <<<EOT
<tr>
<td valign="top" align="left">
    <table border="0" cellspacing="0" cellpadding="0" width="100%">
        <tr>
            <td style="font-family: Arial, sans-serif; font-size: 25px;line-height: 27px;mso-line-height-rule:exactly; color: #444342;" class="main-text">Reset your password</td>
        </tr>
        <tr>
            <td style="padding-top: 20px; font-family: Arial, sans-serif; font-size: 14px;line-height: 16px;mso-line-height-rule:exactly; color: #444342;">
                To reset your password, please follow this verification link:</td>
        </tr>
        <tr>
            <td style="padding-top: 20px; font-family: Arial, sans-serif; font-size: 14px;line-height: 16px;mso-line-height-rule:exactly; color: #444342;">
                <a style="color: #233F72; text-decoration: underline; outline: none;word-wrap: break-word; font-size: 14px;" target="_blank" href="{$link}">{$link}</a></td>
        </tr>
        <tr>
            <td style="padding-top: 20px; font-family: Arial, sans-serif; font-size: 14px;line-height: 16px;mso-line-height-rule:exactly; color: #444342;">This link is valid for 10 minutes only and cannot be reused.</td>
        </tr>
        <tr>
            <td style="padding-top: 20px; font-family: Arial, sans-serif; font-size: 14px;line-height: 16px;mso-line-height-rule:exactly; color: #444342;">
                This is an automated email. If you received this in error, no action is required. 
                If you need any assistance please email <a style="color: #233F72; text-decoration: underline; outline: none; word-wrap: break-word;" target="_blank" href="mailto:helpdesk@dubaicarbon.ae">helpdesk@dubaicarbon.ae</a> 
                    or get in touch via <a style="color: #233F72; text-decoration: underline; outline: none; word-wrap: break-word;" target="_blank" href="{$this->registry('siteUrl')}contact">{$this->registry('siteUrl')}contact</a></td>
        </tr>
        <tr>
            <td style="padding-top: 20px; font-family: Arial, sans-serif; font-size: 14px;line-height: 16px;mso-line-height-rule:exactly; color: #444342;">Kind regards,</td>
        </tr>
        <tr>
            <td style="padding-top: 20px; font-family: Arial, sans-serif; font-size: 14px;line-height: 16px;mso-line-height-rule:exactly; color: #444342;">Dubai Carbon Admin</td>
        </tr>
        <tr>
            <td style="font-family: Arial, sans-serif; font-size: 14px;line-height: 16px;mso-line-height-rule:exactly; color: #444342;"><a style="color: #233F72; text-decoration: underline; outline: none; word-wrap: break-word;" target="_blank" href="http://www.dubaicarbon.ae">www.dubaicarbon.ae</a></td>
        </tr>
    </table>
</td>
</tr>

EOT;
        
        $mailer->send($email, 'DubaiCarbon - Reset Password Verification', $message);
    }
    
    /**
     * check verification code expiry
     * 
     * @param string $expiry
     * @return boolean
     */
    private function _checkExpiry($expiry)
    {
        return time() <= $expiry;
    }

    /**
     * check verification code format
     * 
     * @param string $verification
     * @return boolean
     */
    private function _checkVerificationFormat($verification)
    {
        if (32 == strlen($verification)) {
            return 1 == preg_match('/^[a-f0-9]+$/', $verification);
        }
        return false;
    }
    
    private function _checkBlocked()
    {
        $block = false;
        $file  = STORAGE_DIR . '/cache/blocked.txt';
        if (file_exists($file)) {
            $lines = file($file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            if (! empty($lines)) {
                $remote = Server::remoteAddress();
                foreach ($lines as $ip) {
                    if (0 === strpos($remote, $ip)) {
                        $block = true;
                        break;
                    }
                }
            }
        }
        if ($block) {
            header('HTTP/1.1 403 Forbidden');
            exit;
        }
    }

}
