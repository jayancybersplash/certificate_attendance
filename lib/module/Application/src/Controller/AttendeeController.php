<?php

/**
 * Application Attendee Controller
 * 
 * @category Application
 * @package  Application\Controller
 */

namespace Application\Controller;

use Application\ApplicationController;

use Application\Constant\Tab;
use Application\Constant\FlashMessage;
use Application\Constant\SessionConstant;

use Application\Form\Attendee\CreateForm;
use Application\Form\Attendee\ChangeForm;
use Application\Form\AjaxForm;

use Application\Utility\CertificateWriter;

use Web\Utility\DateHelper;

use Model\Attendee;
use Model\Session;
use Model\Module;
use Model\SessionModule;
use Model\Attendance;
use Model\Certificate;


class AttendeeController extends ApplicationController
{
    /**
     *
     * @var \Model\Session 
     */
    private $_model;

    /**
     *
     * @var int 
     */
    private $_id;
    
    /**
     *
     * @var \stdClass
     */
    private $_session;

    public function __construct() 
    {
        parent::__construct();
        $this->loginRequired();

        $this->_model = new Session();
        $this->setHeadTitle('Sessions');
        $this->setPageSubheading('Attendees');
        $this->currentTab(Tab::SESSION);
        $this->setFlashMessageTitle('Attendee');
    }

    /**
     * index action
     * 
     * @return \Web\View\ViewModel
     */
    public function indexAction() 
    {
        $this->_loadSession();
        
        $attendeeModel = new Attendee();
        $sql = $this->_model->getSql();
        $where = $sql->whereId('session_id', $this->_id)
                     ->getWhere();
        
        $total = $attendeeModel->selectTotal($where);
        if ($total > 0) {
            $this->view->setVariable('list', $attendeeModel->select($where, 'first_name'));
            $this->addDatatableScripts();
        }
        
        $this->view->setVariable('total', $total);
        $this->_breadcrumb('Attendees');
        $this->_setTab();

        return $this->getViewModel();
    }

    /**
     * create action
     * 
     * @return \Web\View\ViewModel
     */
    public function createAction() 
    {
        $this->_loadSession();
        $this->_checkLock();
        $attendeeModel = new Attendee();
        $form = new CreateForm();
        $form->setModel($attendeeModel);
        if ($form->isPost()) {
            if ($form->isValid($this->_id)) {
                $form->create($this->getUserId(), $this->_id);
                $this->setActionFlashMessage(FlashMessage::CREATE);
                $this->_goToList();
            }
        }
        
        $this->registerForm($form);
        $this->plugin('script')->addModuleBody('attendee');
        $this->_breadcrumb('New Attendee');
        $this->_setTab();

        return $this->getViewModel();
    }

    /**
     * change action
     * 
     * @return \Web\View\ViewModel
     */
    public function changeAction() 
    {
        $this->_loadSession();
        $this->_checkLock();
        $attendeeId = $this->_getAttendeeId();
        
        $attendeeModel = new Attendee();
        $form = new ChangeForm();
        $form->setModel($attendeeModel);
        $form->setId($attendeeId);
        if ($form->isPost()) {
            if ($form->isValid($this->_id)) {
                if ($form->change()) {
                    $this->setActionFlashMessage(FlashMessage::CHANGE);
                } else {
                    $this->setActionFlashMessage(FlashMessage::NO_CHANGE);
                }
                $this->redirect('attendee/index/id/' . $this->_id);
            }
        } else {
            $form->populate($attendeeModel->find($attendeeId));
        }
        $this->registerForm($form);
        $this->_breadcrumb('Edit Attendee');
        $this->plugin('script')->addModuleBody('attendee');
        $this->_setTab();

        return $this->getViewModel();
    }
    
    /**
     * view action
     * 
     * @return \Web\View\ViewModel
     */
    public function viewAction() 
    {
        $this->_loadSession();
        $attendeeId = $this->_getAttendeeId();
        
        $attendeeModel = new Attendee();
        $attendee = $attendeeModel->find($attendeeId);
        
        $attendance = [];
        $fullAttendance = false;
        $sessionModuleModel = new SessionModule();
        $sql = $this->_model->getSql();
        $sql->whereId('session_id', $this->_id);
        $sessionModules = $sessionModuleModel->selectPairs(['id', 'module_id'], $sql->getWhere());
        $moduleModel = new Module();
        $modules = $moduleModel->select();
        foreach ($modules as $item) {
            if (in_array($item->id, $sessionModules)) {
                $sesionModuleId = array_search($item->id, $sessionModules);
                $attendance[$sesionModuleId] = [
                    'id' => $item->id,
                    'code' => $item->code,
                    'title' => $item->title,
                    'attendance' => 0,
                ];
            }
        }
        
        $sessionModuleCount = count($attendance);
        $summary = '0 / ' . $sessionModuleCount;
        $sql->clear();
        $sql->whereId('attendee_id', $attendeeId);
        
        $where = $sql->getWhere();
        $attendanceModel = new Attendance();
        $attendedModules = $attendanceModel->selectColumns('session_module_id', $where);
        if (! empty($attendedModules)) {
            foreach ($attendedModules as $sessionModuleId) {
                $attendance[$sessionModuleId]['attendance'] = 1;
            }
            if (count($attendedModules) == $sessionModuleCount) {
                $fullAttendance = true;
                $summary = $sessionModuleCount . ' / ' . $sessionModuleCount;
            } else {
                $summary = count($attendedModules) . ' / ' . $sessionModuleCount;
            }
        }
        
        $actions = [];
        if ($fullAttendance) {
            $hasTemplate = '' != $this->_session->certificate_template;
            $certificateModel = new Certificate();
            if ($certificateModel->selectTotal($where) > 0) {
                $this->view->setVariable('certificate', $certificateModel->selectLastRow($where, 'id DESC'));
                if ($hasTemplate) {
                    $actions[SessionConstant::CERTIFICATE_RE_GENERATE] = 'Re-generate Certificate';
                    $actions[SessionConstant::CERTIFICATE_REMOVE] = 'Delete Certificate';
                }
            } elseif ($hasTemplate) {
                $actions[SessionConstant::CERTIFICATE_GENERATE] = 'Generate Certificate';
            }
        }
        
        $this->view->setVariable('summary', $summary);
        $this->view->setVariable('attendee', $attendee);
        $this->view->setVariable('attendance', $attendance);
        $this->view->setVariable('fullAttendance', $fullAttendance);
        $this->view->setVariable('actions', $actions);
        
        $this->_breadcrumb('View Attendee');
        $this->plugin('script')->addModuleBody('attendee-view');
        if (0 == $this->_session->locked) {
            $this->layout->addModal('attendee/attendance');
        }
        $this->_setTab();

        return $this->getViewModel();
    }
    
    /**
     * certificate action
     * 
     * @return \Web\View\ViewModel
     */
    public function certificateAction() 
    {
        $this->_loadSession();
        $attendeeId = $this->_getAttendeeId();
        
        if ($this->isAjaxRequest()) {
            $response = $this->getAjaxResponse();
            $message = 'Invalid input';
            $form = new AjaxForm();
            $form->addField('action');
            if ($form->isPost()) {
                $values = $form->run();
                $actions = [];
                if ('' != $this->_session->certificate_template) {
                    $attendeeModel = new Attendee();
                    $attendee = $attendeeModel->find($attendeeId);
                    $sessionModuleModel = new SessionModule();
                    $sql = $this->_model->getSql();
                    $sql->whereId('session_id', $this->_id);
                    $sessionModuleCount = $sessionModuleModel->selectTotal($sql->getWhere());

                    $sql->clear();
                    $sql->whereId('attendee_id', $attendeeId);
                    $where = $sql->getWhere();
                    $attendanceModel = new Attendance();
                    $fullAttendance = $sessionModuleCount == $attendanceModel->selectTotal($where);

                    if ($fullAttendance) {
                        $certificateModel = new Certificate();
                        if ($certificateModel->selectTotal($where) > 0) {
                            $actions = [
                                SessionConstant::CERTIFICATE_RE_GENERATE,
                                SessionConstant::CERTIFICATE_REMOVE,
                            ];
                        } else {
                            $actions[] = SessionConstant::CERTIFICATE_GENERATE;
                        }
                    }
                    if (! empty($actions)) {
                        $message = 'Action not permitted';
                        $action = (int) $values['action'];
                        if ($action > 0) {
                            if (in_array($action, $actions)) {
                                try {
                                    $this->_model->getAdapter()->beginTransaction();
                                    switch ($action) {
                                        case SessionConstant::CERTIFICATE_GENERATE:
                                        case SessionConstant::CERTIFICATE_RE_GENERATE:
                                            $filename = $this->_generateCertificate($attendee);
                                            if (false !== $filename) {
                                                $insert = [
                                                    'attendee_id' => $attendeeId,
                                                    'file_name' => $filename, 
                                                    'created_on' => DateHelper::now(),
                                                ];
                                                $certificateModel->insert($insert);
                                                $attendeeModel->updateById(['certificate_generated' => 1], $attendeeId);
                                                $response->valid();
                                                $message = 'success';
                                                if (SessionConstant::CERTIFICATE_GENERATE == $action) {
                                                    $this->setFlashMessage('Certificate generated');
                                                } else {
                                                    $this->setFlashMessage('Certificate re-generated');
                                                }
                                            }
                                            break;

                                        case SessionConstant::CERTIFICATE_REMOVE:
                                            $certificateModel->deleteByIdColumn('attendee_id', $attendeeId);
                                            $attendeeModel->updateById(['certificate_generated' => 0], $attendeeId);
                                            $this->setFlashMessage('Certificate deleted');
                                            $response->valid();
                                            $message = 'success';
                                            break;

                                        default:
                                            break;
                                    }
                                    $this->_model->getAdapter()->commit();
                                } catch (\PDOException $ex) {
                                    //echo $ex->getMessage(); exit;
                                    $this->_model->getAdapter()->rollBack();
                                    $message = 'Action failed';
                                }
                            }
                        }
                    }
                }
            }
            
            $response->message($message);
        }
        
        return $this->getViewModel();
    }
    
    /**
     * attendance action
     * 
     * @return \Web\View\ViewModel
     */
    public function attendanceAction() 
    {
        $this->_loadSession();
        $attendeeId = $this->_getAttendeeId();
        if (0 == $this->_session->locked) {
            if ($this->isAjaxRequest()) {
                $response = $this->getAjaxResponse();
                $message = 'Invalid input';
                $form = new AjaxForm();
                $form->addField('session_module');
                $form->multiselect(['session_module']);
                if ($form->isPost()) {
                    $values = $form->run();
                    $selected = $values['session_module'];
                    $attendanceModel = new Attendance();
                    $sql = $this->_model->getSql();
                    $sql->whereId('attendee_id', $attendeeId);
                    $where = $sql->getWhere();
                    $current = (array) $attendanceModel->selectPairs(['id', 'session_module_id'], $where);
                    try {
                        $this->_model->getAdapter()->beginTransaction();
                        if (empty($selected)) {
                            if (! empty($current)) {
                                $attendanceModel->delete($where);
                            }
                        } else {
                            if (empty($current)) {
                                $insert = $attendanceModel->getInsertPreparedStatement(['session_module_id', 'attendee_id']);
                                foreach ($selected as $sessionModuleId) {
                                    $insert->execute([$sessionModuleId, $attendeeId]);
                                }
                            } else {
                                $new = array_diff($selected, $current);
                                $deleted = array_diff($current, $selected);
                                if (! empty($deleted)) {
                                    $attendanceModel->deleteByIds(array_keys($deleted));
                                }

                                if (! empty($new)) {
                                    $insert = $attendanceModel->getInsertPreparedStatement(['session_module_id', 'attendee_id']);
                                    foreach ($new as $sessionModuleId) {
                                        $insert->execute([$sessionModuleId, $attendeeId]);
                                    }
                                }
                            }
                        }
                    
                        $attendeeModel = new Attendee();
                        $attendee = $attendeeModel->find($attendeeId);
                        if ($attendee->certificate_generated > 0) {
                            $sessionModuleModel = new SessionModule();
                            $sql->clear();;
                            $sql->whereId('session_id', $this->_id);
                            $sessionModuleCount = $sessionModuleModel->selectTotal($sql->getWhere());
                            if (count($selected) < $sessionModuleCount) {
                                $certificateModel = new Certificate();
                                $sql->clear();;
                                $sql->whereId('attendee_id', $attendeeId);
                                $where2 = $sql->getWhere();
                                if ($certificateModel->selectTotal($where2) > 0) {
                                    $certificateModel->deleteByIdColumn('attendee_id', $attendeeId);
                                }
                                $attendeeModel->updateById(['certificate_generated' => 0], $attendeeId);
                            }
                        }
                        $this->_model->getAdapter()->commit();
                        $this->setFlashMessage('Attendance updated');
                        $response->valid();
                        $message = 'success';
                    } catch (\PDOException $ex) {
                        //echo $ex->getMessage(); exit;
                        $this->_model->getAdapter()->rollBack();
                        $message = 'Unable to update attendance';
                    }
                }

                $response->message($message);
            }
        }
        
        return $this->getViewModel();
    }
    
    /**
     * remove action
     * 
     * @return \Web\View\ViewModel
     */
    public function removeAction() 
    {
        $this->_loadSession();
        $attendeeId = $this->_getAttendeeId();

        if (0 == $this->_session->locked) {
            $attendeeModel = new Attendee();
            $this->checkDeleteRequest('attendee');
            $attendeeModel->deleteById($attendeeId);
            $this->setActionFlashMessage(FlashMessage::REMOVE);
        }
        
        $this->_goToList();
        
        return $this->getViewModel();
    }

    /**
     * get session id 
     * 
     * @return int
     */
    private function _getId() 
    {
        $valid = false;
        $id = $this->getParam('id', true);
        if ($id > 0) {
            $valid = $this->_model->hasId($id);
        }
        if (!$valid) {
            $this->setFlashMessage('Invalid request', 'session', 'error');
        }
        $this->_id = $id;

        return $id;
    }

    /**
     * get attendee id
     * 
     * @return int
     */
    private function _getAttendeeId() 
    {
        $valid = false;
        $attendeeModel = new Attendee();
        $id = $this->getParam('attendee-id', true);
        if ($id > 0) {
            if ($attendeeModel->hasId($id)) {
                $valid = $this->_id == $attendeeModel->selectColumnById('session_id', $id);
            }
        }
        if (! $valid) {
            $this->setFlashMessage('Invalid request', 'attendee/index/id/' . $this->_id, 'error');
        }

        return $id;
    }

    /**
     * set page breadcrumb
     * 
     * @param string $page
     */
    private function _breadcrumb($page = null)
    {
        $breadrumb = $this->plugin('breadcrumb');
        $breadrumb->add('Sessions', $this->view->actionUrl('session'));
        if (null !== $page) {
            $breadrumb->add($page);
        }
    }
    
    /**
     * load session
     */
    private function _loadSession()
    {
        $this->_getId();
        $this->_session = $this->_model->find($this->_id);
    }
    
    /**
     * set tab and view variables
     */
    private function _setTab()
    {
        $this->view->setVariable('sessionId', $this->_id);
        $this->view->setVariable('session', $this->_session);
        $this->view->setVariable('tab', SessionConstant::TAB_ATTENDEE);
    }
    
    /**
     * check session lock
     */
    private function _checkLock()
    {
        if ($this->_session->locked > 0) {
            $this->setFlashMessage('Session locked', 'attendee/index/id/' . $this->_id, 'error');
        }
    }
    
    /**
     * go to attendees list
     */
    private function _goToList()
    {
        $this->redirect('attendee/index/id/' . $this->_id);
    }
    
    /**
     * generate certificate
     * 
     * @param \stdClass $attendee
     * @return boolean|string
     */
    private function _generateCertificate($attendee)
    {
        $certificate = new CertificateWriter();
        $certificate->setName($attendee->name_on_certificate)
                    ->setTemplate($this->_session->certificate_template);
        return $certificate->run($this->_id);
    }

}
