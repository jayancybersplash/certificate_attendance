<?php
/**
 * Application Module config
 */

return [
    'session' => 'default',
    'view' => 'view',
    'plugins' => [],
];
