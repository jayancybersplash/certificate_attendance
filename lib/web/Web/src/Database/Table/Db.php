<?php
/**
 * Database Table Db
 * 
 * @category Web
 * @package  Web\Database
 * @category Web\Database\Table
 */

namespace Web\Database\Table;


final class Db
{
    const ATTENDANCE = 'attendance';
    const ATTENDEE = 'attendee';
    const CERTIFICATE = 'certificate';
    const MODULE = 'module';
    const SESSION = 'session';
    const SESSION_MODULE = 'session_module';
    const USER = 'user';
    const USER_TYPE = 'user_type';
    
    public function __construct() { }
    
}
