<?php
/**
 * Validator Interface
 *
 * @category   Web
 * @package    Web\Form
 */

namespace Web\Form;


interface ValidatorInterface
{
    public function isValid($string);
  
}
