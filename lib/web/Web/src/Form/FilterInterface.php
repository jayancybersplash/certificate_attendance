<?php
/**
 * Filter Interface
 *
 * @category   Web
 * @package    Web\Form
 */

namespace Web\Form;


interface FilterInterface
{
    public function filter($string);
  
}
