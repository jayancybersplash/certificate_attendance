<?php
/**
 * Web Security Password
 *
 * @category   Web
 * @package    Web\Security
 */

namespace Web\Security;


class Password
{
    const AUTHORIZATION_SALT = '$K6&*WYtBH38}vQ24RkgF~3wXzp:9sQw[dpcfc]';
    
    /**
     * match password
     * 
     * @param string $input
     * @param string $base
     * @return boolean
     */
    public static function match($input, $base)
    {
        return password_verify($input, $base);
    }
    
    /**
     * encrypt password
     * 
     * @param string $input
     * @return string
     */
    public static function encrypt($input)
    {
        return password_hash($input, PASSWORD_BCRYPT);
    }
    
    /**
     * encrypt admin password
     * 
     * @param string $input
     * @return string
     */
    public static function adminEncrypt($input)
    {
        return md5(md5($input) . self::AUTHORIZATION_SALT);
    }
    
    /**
     * match admin password
     * 
     * @param string $input
     * @param string $base
     * @return boolean
     */
    public static function matchAdmin($input, $base)
    {
        return 0 === strcmp(self::adminEncrypt($input), $base);
    }
    
}
