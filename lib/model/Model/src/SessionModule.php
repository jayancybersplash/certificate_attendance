<?php
/**
 * SessionModule Model
 *
 * @category   Model
 */

namespace Model;

use Web\Database\TableGateway;
use Web\Database\Table\Db;


class SessionModule extends TableGateway
{
    public function __construct()
    {
        parent::__construct(Db::SESSION_MODULE);
    }
    
    
}
