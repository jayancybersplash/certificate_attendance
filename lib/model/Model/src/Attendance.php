<?php
/**
 * Attendance Model
 *
 * @category   Model
 */

namespace Model;

use Web\Database\TableGateway;
use Web\Database\Table\Db;


class Attendance extends TableGateway
{
    public function __construct()
    {
        parent::__construct(Db::ATTENDANCE);
    }

}
