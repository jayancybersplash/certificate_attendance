<?php
/**
 * UserType Model
 *
 * @category   Model
 */

namespace Model;

use Web\Database\TableGateway;
use Web\Database\Table\Db;


class UserType extends TableGateway
{
    public function __construct()
    {
        parent::__construct(Db::USER_TYPE);
    }

}
