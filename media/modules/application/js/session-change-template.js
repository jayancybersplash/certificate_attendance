"use strict";

var SessionChangeTemplate = {
    
    ready: function () {
        $("form#form-session-change-template").submit(SessionChangeTemplate.validateForm);
    },
    
    validateForm: function () {
        if ($("input#template").get(0).files.length > 0) {
            var templateFile = $("input#template").get(0).files[0];
            if (! /\.pdf$/i.test(templateFile.name) || "application/pdf" !== templateFile.type) {
                alert("Template file should be in PDF format");
                
                return false;
            }
        } else {
            alert("Please upload template file");
        }
        
        return true;
    }
};

$(document).ready(function() {
        SessionChangeTemplate.ready();
    }
);
