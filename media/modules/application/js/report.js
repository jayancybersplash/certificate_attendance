"use strict";

var ApplicationReport = {
    reportTypeAttendance: 2,
    ready: function() {
        $("select#session_id").change(ApplicationReport.loadReportOptions);
        $("select#type").change(ApplicationReport.loadReportOptions);
        $("form#form-report").submit(ApplicationReport.validateForm);
    },
    
    loadReportOptions: function () {
        if ($("select#session_id").val() > 0) {
            if (ApplicationReport.reportTypeAttendance == $("select#type").val()) {
                ApplicationReport.loadModules();
            } else {
                $("div#select-module").hide();
            }
        } else {
            $("div#select-module").hide();
        }
    },
    
    loadModules: function () {
        var formAction = $("input#get-modules-url").val();
        var formData = { 
            id : $("select#session_id").val()
        };
        $.post(formAction, formData, function(reply) {
                if (reply.success && reply.valid) {
                    $("input.session-module").prop("disabled", true).prop("checked", false);
                    if (reply.modules) {
                        $.each(reply.modules, function (key, moduleId) {
                                $("input#module-" + moduleId).prop("disabled", false).prop("checked", true);
                            }
                        );
                        $("div#select-module").show();
                    }
                } else if (reply.redirect) {
                    ApplicationCommon.reloadPage();
                } else if (reply.message) {
                    alert(reply.message);
                } else {
                    alert("Unable to complete the request.");
                }
            }, "json"
        );
    },
    
    validateForm: function () {
        if (0 == $("select#session_id").val()) {
            alert("Please select session");
            return false;
        }
        if (0 == $("input.attendee-field:checked").length) {
            alert("Please select attendee fields");
            return false;
        }
        if (ApplicationReport.reportTypeAttendance == $("select#type").val()) {
            if (0 == $("input.session-module:checked").length) {
                alert("Please select module");
                return false;
            }
        }
        
        return true;
    }

};

$(document).ready(function() {
        ApplicationReport.ready();
    }
);
