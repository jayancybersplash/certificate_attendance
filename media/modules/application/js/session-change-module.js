"use strict";

var SessionChangeModule = {
    ready: function () {
        $("form#form-session-change-module").submit(SessionChangeModule.validateForm);
    },
        
    validateForm: function () {
        if (0 === $("input[name^='module']:checked").length) {
            alert("Please select modules");
            return false;
        }
        
        return true;
    }
};

$(document).ready(function () {
        SessionChangeModule.ready();
    }
);
