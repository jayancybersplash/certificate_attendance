"use strict";

var ApplicationAttendee = {
    ready: function () {
        $("input#first_name,input#last_name").blur(ApplicationAttendee.updateNameOnCertificate);
        $("input#name_on_certificate").blur(ApplicationAttendee.fixNameOnCertificate);
    },
    
    updateNameOnCertificate: function () {
        var nameOnCertificate = $.trim($("input#first_name").val());
        nameOnCertificate += " " + $.trim($("input#last_name").val());
        $("input#name_on_certificate").val(nameOnCertificate.toUpperCase());
        
        return false;
    },
    
    fixNameOnCertificate: function () {
        $(this).val(($.trim($(this).val())).toUpperCase());
        
        return false;
    }

};

$(document).ready(function () {
        ApplicationAttendee.ready();
    }
);
