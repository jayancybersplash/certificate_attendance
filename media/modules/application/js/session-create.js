"use strict";

var SessionCreate = {
    datepickerDateFormat: "",
    dateIndex: { d : 0, m : 1, y : 2},
    dateSeparator: "/",
    ready: function () {
        SessionCreate.datepickerDateFormat = datepickerDateFormat;
        SessionCreate.datepicker();
        $("form#form-session-create").submit(SessionCreate.validateForm);
        if (-1 === datepickerDateFormat.indexOf(SessionCreate.dateSeparator)) {
            if (-1 !== datepickerDateFormat.indexOf("-")) {
                SessionCreate.dateSeparator = "-";
            } else {
                SessionCreate.dateSeparator = "/";
            }
        }
    },
    
    datepicker: function () {
        $("input#start_date,input#end_date").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: SessionCreate.datepickerDateFormat,
            }
        });
    },
    
    validateForm: function () {
        var startDate = $("input#start_date").val();
        var endDate = $("input#end_date").val();
        if (startDate != endDate) {
            var dateParts1 = startDate.split(SessionCreate.dateSeparator);
            var dateParts2 = endDate.split(SessionCreate.dateSeparator);
            var startDateParts = [ dateParts1[SessionCreate.dateIndex.y], dateParts1[SessionCreate.dateIndex.m], dateParts1[SessionCreate.dateIndex.d] ];
            var endDateParts = [ dateParts2[SessionCreate.dateIndex.y], dateParts2[SessionCreate.dateIndex.m], dateParts2[SessionCreate.dateIndex.d] ];
            var startDateObject = new Date(startDateParts.join("-"));
            var endDateObject = new Date(endDateParts.join("-"));
            if (endDateObject.getTime() < startDateObject.getTime()) {
                alert("End date should be on or after Start date");
                
                return false;
            }
        }
        
        if (0 === $("input[name^='module']:checked").length) {
            alert("Please select modules");
            return false;
        }
        
        if ($("input#template").get(0).files.length > 0) {
            var templateFile = $("input#template").get(0).files[0];
            if (! /\.pdf$/i.test(templateFile.name) || "application/pdf" !== templateFile.type) {
                alert("Template file should be in PDF format");
                
                return false;
            }
        }
        
        return true;
    }
};

$(document).ready(function () {
        SessionCreate.ready();
    }
);
