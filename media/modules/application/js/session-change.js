"use strict";

var SessionChange = {
    datepickerDateFormat: "",
    dateIndex: { d : 0, m : 1, y : 2},
    dateSeparator: "/",
    ready: function () {
        SessionChange.datepickerDateFormat = datepickerDateFormat;
        SessionChange.datepicker();
        $("form#form-session-change").submit(SessionChange.validateForm);
        if (-1 === datepickerDateFormat.indexOf(SessionChange.dateSeparator)) {
            if (-1 !== datepickerDateFormat.indexOf("-")) {
                SessionChange.dateSeparator = "-";
            } else {
                SessionChange.dateSeparator = "/";
            }
        }
    },
    
    datepicker: function () {
        $("input#start_date,input#end_date").daterangepicker({
            singleDatePicker: true,
            showDropdowns: true,
            locale: {
                format: SessionChange.datepickerDateFormat,
            }
        });
    },
    
    validateForm: function () {
        var startDate = $("input#start_date").val();
        var endDate = $("input#end_date").val();
        if (startDate != endDate) {
            var dateParts1 = startDate.split(SessionChange.dateSeparator);
            var dateParts2 = endDate.split(SessionChange.dateSeparator);
            var startDateParts = [ dateParts1[SessionChange.dateIndex.y], dateParts1[SessionChange.dateIndex.m], dateParts1[SessionChange.dateIndex.d] ];
            var endDateParts = [ dateParts2[SessionChange.dateIndex.y], dateParts2[SessionChange.dateIndex.m], dateParts2[SessionChange.dateIndex.d] ];
            var startDateObject = new Date(startDateParts.join("-"));
            var endDateObject = new Date(endDateParts.join("-"));
            if (endDateObject.getTime() < startDateObject.getTime()) {
                alert("End date should be on or after Start date");
                return false;
            }
        }

        return true;
    }
};

$(document).ready(function() {
        SessionChange.ready();
    }
);
