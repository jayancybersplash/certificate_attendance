"use strict";

var SessionChangeLock = {
    
    ready: function () {
        $("form#form-session-change-lock").submit(function () {
            if ($("input#locked").val() > 0) {
                return confirm("Are you sure you to Unlock the session and grant change permissions?");
            }
            
            return confirm("Are you sure you to Lock the session and prevent modifications?");
        });
    }
};

$(document).ready(function() {
        SessionChangeLock.ready();
    }
);
