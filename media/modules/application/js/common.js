"use strict";

var ApplicationCommon = {
    ready: function() {
        ApplicationCommon.confirmRemove();
        ApplicationCommon.cancelButton();
        ApplicationCommon.numberDecimal();
        ApplicationCommon.numberInteger();
        $.ajaxSetup({
            headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content") }
        }); 
    },
    
    
    confirmRemove: function() {
        $("a.confirm-remove").click(function() {
                return confirm("Are you sure you want to delete this " + $(this).data("confirm") + "?");
            }
        );
    },
    
    cancelButton: function() {
        $("button.form-button-cancel").click(function() {
                window.location = $(this).data("cancel");
            }
        );
    },
    
    reloadPage: function() {
        window.location.reload(true);
    }, 
    
    numberDecimal: function() {
        $("input[type='text'].number-decimal").on("keypress", function(e) {
            if (e.ctrlKey) {
                return;
            }
            if (e.key.length > 1) {
                return;
            }
            var code = (e.which) ? e.which : e.keyCode;
            if (code === 46 || (code > 47 && code < 58)) {
                var currentValue = $(this).val();
                if (code === 46) {
                    if ("" === currentValue) {
                        e.preventDefault();
                        return false;
                    }
                    if (-1 !== currentValue.indexOf(".")) {
                        e.preventDefault();
                        return false;
                    }
                } else if (code === 48) {
                    if ("0" === currentValue) {
                        e.preventDefault();
                        return false;
                    }
                }
            } else {
                e.preventDefault();
                return false;
            }
            return true;
        });
        
        //.on("paste", function(e) { 
        //    e.preventDefault(); return false; 
        //})
    },
    
    numberInteger: function() {
        $("input[type='text'].number-integer").on("keypress", function(e) {
            if (e.ctrlKey) {
                return;
            }
            if (e.key.length > 1) {
                return;
            }
            var code = (e.which) ? e.which : e.keyCode;
            var currentValue = $(this).val();
            if (code > 47 && code < 58) {
                if (code === 48) {
                    if ("0" === currentValue) {
                        e.preventDefault();
                        return false;
                    }
                }
            } else {
                e.preventDefault();
                return false;
            }
            return true;
        });
        $("input[type='number'].number-integer").on("keypress", function(e) {
            if (e.ctrlKey) {
                return;
            }
            if (e.key.length > 1) {
                return;
            }
            var maxLength = $(this).attr("maxlength");
            var currentValue = $(this).val();
            if (maxLength) {
                if (currentValue.length >= maxLength) {
                    e.preventDefault();
                    return false;
                }
            }
            return true;
        });
    }
    
};

$(document).ready(function() {
        ApplicationCommon.ready();
    }
);
