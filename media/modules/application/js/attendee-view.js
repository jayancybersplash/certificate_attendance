"use strict";

var ApplicationAttendeeView = {
    ready: function() {
        $("a#edit-attendance").click(ApplicationAttendeeView.showAttendanceForm);
        $("form#form-certificate-action").submit(ApplicationAttendeeView.certificateAction);
        $("a#edit-attendance").click(ApplicationAttendeeView.showAttendanceForm);
        $("form#form-attendance-modal").submit(ApplicationAttendeeView.saveAttendance);
    },
    
    showAttendanceForm: function () {
        document.getElementById("form-attendance-modal").reset();
        $("div#modal-attendance").modal("show");
        
        return false;
    },
    
    certificateAction: function () {
        var selectedAction = $("select#certificate_action").val();
        if (selectedAction > 0) {
            var formAction = $("input#certificate-action-url").val();
            var formData = { 
                action : selectedAction
            };
            $.post(formAction, formData, function(reply) {
                    if (reply.success && reply.valid) {
                        ApplicationCommon.reloadPage();
                    } else if (reply.redirect) {
                        ApplicationCommon.reloadPage();
                    } else if (reply.message) {
                        alert(reply.message);
                    } else {
                        alert("Unable to complete the request.");
                    }
                }, "json"
            );
        } else {
            alert("Please select action");
        }
        
        return false;
    },
    
    saveAttendance: function() {
        var sessionModuleIds = [];
        $("form#form-attendance-modal input[type=checkbox]:checked").each( function () {
            sessionModuleIds.push($(this).val());
        });
        var formAction = $("input#attendance-update-url").val();
        var formData = { 
            session_module : sessionModuleIds
        };
        $.post(formAction, formData, function(reply) {
                if (reply.success && reply.valid) {
                    $("div#modal-attendance").modal("hide");
                    ApplicationCommon.reloadPage();
                } else if (reply.redirect) {
                    ApplicationCommon.reloadPage();
                } else if (reply.message) {
                    alert(reply.message);
                } else {
                    alert("Unable to complete the request.");
                }
            }, "json"
        );
        
        return false;
    }
    
};

$(document).ready(function() {
        ApplicationAttendeeView.ready();
    }
);
