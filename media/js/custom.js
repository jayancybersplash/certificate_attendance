/**
 * custom scripts
 * 
 * @type text/javascript
 */
$(document).ready(function() {
    $(".icon").click(function() {
        var current = $('ul#hmenu');
        var height = current.height();
        var newHeight = Math.round(height/2);
        if ($('ul#hmenu').outerHeight() > $(window).outerHeight()) {
            var current = $('ul#hmenu');
            current.removeAttr('style').css({
                overflowY: 'auto',
                height: '100%'
            });
        } else {
            current.removeAttr('style').css({
                position: 'relative',
                top: '50%',
                marginTop: -1 * newHeight
            });        
        }
        return false;
    });
    if ($(window).outerWidth() > 767) {
        $("img#jqImg").on('load', function() {
            $('#greyBox').css({
                'height': $('#greyboxNeighbour').outerHeight()
            });
        });
        var divHeight = $('#greyboxNeighbour').outerHeight();
        $('#greyBox').css('height', divHeight + 'px');
    }
    if ($('.child').outerHeight() > $('.parent').outerHeight()) {
        $('.child').css({
            top : 0,
            left : 0,
            width: '100%',
            transform: 'none',
            padding: '3% 7%',
            margin: '2% 0'
        });
        return false;
    }
    
    if ($(window).outerWidth() > 767) {
        $("img#jqImg2").on('load', function() {
            $('#greyBox2').css({
                'height': $('#greyboxNeighbour2').outerHeight()
            });
        });
        var divHeight = $('#greyboxNeighbour2').outerHeight();
        $('#greyBox2').css('height', divHeight + 'px');
    }
    if ($('#greyBox2 .child').outerHeight() > $('.parent').outerHeight()) {
        $('#greyBox2 .child').css({
            top : 0,
            left : 0,
            width: '100%',
            transform: 'none',
            padding: '3% 7%',
            margin: '2% 0'
        });
        return false;
    }
    $(".enter-search-xs-btn").click(function() {
        $('.enter-search-terms').slideToggle();
        $('a.show-arrow').toggleClass('toggle-arrow');
    });
});

$(window).resize(function() {
    var current = $('ul#hmenu');
    var height = current.height();
    var newHeight = Math.round(height/2);
    if ($('ul#hmenu').outerHeight() > $(window).outerHeight()) {
        var current = $('ul#hmenu');
        current.removeAttr('style');current.css({
            overflowY: 'auto',
            height:'100%'
        });
    } else{
        current.removeAttr('style').css({
            position: 'relative',
            top: '50%',
            marginTop: -1 * newHeight
        });        
    }
    if ($(window).outerWidth() > 767) {
        $("img#jqImg").on('load', function() {
            $('#greyBox').css({
            'height': $('#greyboxNeighbour').outerHeight()
        });
        });
        var divHeight = $('#greyboxNeighbour').outerHeight();
        $('#greyBox').css('height', divHeight + 'px');
    }
    if ($('.child').outerHeight() > $('.parent').outerHeight()) {
        $('.child').css({
            top : 0,
            left : 0,
            width: '100%',
            transform: 'none',
            padding: '3% 7%',
            margin: '2% 0'
        });
    } else {
        $('.child').removeAttr('style');
    }

    if ($(window).outerWidth() > 767) {
        $("img#jqImg2").on('load', function() {
            $('#greyBox2').css({
                'height': $('#greyboxNeighbour2').outerHeight()
            });
        });
        var divHeight = $('#greyboxNeighbour2').outerHeight();
        $('#greyBox2').css('height', divHeight + 'px');
    }
    if ($('#greyBox2 .child').outerHeight() > $('.parent').outerHeight()) {
        $('#greyBox2 .child').css({
            top : 0,
            left : 0,
            width: '100%',
            transform: 'none',
            padding: '3% 7%',
            margin: '2% 0'
        });
    } else {
        $('#greyBox2 .child').removeAttr('style');
    }
    return false;
});

// Custom Scrollbar
(function($) {
    $(window).on("load", function () {
        $(".content").mCustomScrollbar();
    });
})(jQuery);
